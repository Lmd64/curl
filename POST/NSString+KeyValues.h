//
//  NSString+KeyValues.h
//  curl
//
//  Created by Liam Dunne on 23/04/2014.
//  Copyright (c) 2014 Lmd64. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Flat)
- (NSString *)flatten;
- (NSString *)jsonStringWithPrettyPrint:(BOOL)prettyPrint;
- (NSString *)stringFromObject;
- (id)safeValueForKeyPath:(NSString *)keyPath;
@end


@interface NSString (KeyValues)
- (NSString *)compactString;
- (BOOL)validKeyPathForDictionary:(NSDictionary *)dictionary;
- (NSString *)keyPath;
- (NSAttributedString *)attributedStringWithColor:(UIColor *)color;
- (NSAttributedString *)attributedStringWithColor:(UIColor *)color backgroundColor:(UIColor *)backgroundColor;
@end
