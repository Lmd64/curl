//
//  UIView+VisualDebug.h
//  Homestay
//
//  Created by Liam Dunne on 09/05/2014.
//  Copyright (c) 2014 Liam Dunne. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (VisualDebug)
- (void)debugViewBorders;
- (void)debugBorders;
@end
