//
//  NSObject+MethodExchange.m
//  Homestay
//
//  Created by Liam Dunne on 02/01/2014.
//  Copyright (c) 2014 Liam Dunne. All rights reserved.
//

#import <objc/runtime.h>
#import "NSObject+Extensions.h"
#import "NSString+Extensions.h"

@implementation NSObject (Extensions)
+ (void)exchangeMethod:(SEL)origSel withNewMethod:(SEL)newSel {
	Class class = [self class];

	Method origMethod = class_getInstanceMethod(class, origSel);
	if (!origMethod) {
		origMethod = class_getClassMethod(class, origSel);
	}
	if (!origMethod)
		@throw [NSException exceptionWithName:@"Original method not found" reason:nil userInfo:nil];
	Method newMethod = class_getInstanceMethod(class, newSel);
	if (!newMethod) {
		newMethod = class_getClassMethod(class, newSel);
	}
	if (!newMethod)
		@throw [NSException exceptionWithName:@"New method not found" reason:nil userInfo:nil];
	if (origMethod == newMethod)
		@throw [NSException exceptionWithName:@"Methods are the same" reason:nil userInfo:nil];
	method_exchangeImplementations(origMethod, newMethod);
}
- (NSString *)stringFromObject {
	id value = self;
	if ([value isKindOfClass:[NSString class]]) {
		value = [[value flatten] compactString];
	}
	else if ([value isKindOfClass:[NSArray class]]) {
		if ([(NSArray*)value count]) {
			value = [[value firstObject] flatten];
		}
		else {
			value = [[value flatten] compactString];
		}
	}
	else if ([value isKindOfClass:[NSDictionary class]]) {
		value = [[value flatten] compactString];
    } else if ([value isKindOfClass:[NSNumber class]]){
        if (strcmp([value objCType],@encode(int))){
            value = [@([value integerValue]) stringValue];
        } else if (strcmp([value objCType],@encode(float))){
            value = [@([value floatValue]) stringValue];
        } else if (strcmp([value objCType],@encode(double))){
            value = [@([value doubleValue]) stringValue];
        }
    }
	else {
		value = [[[value description] flatten] compactString];
	}
	return value;
}
- (NSString *)flatten {
	NSString *flatten = [self description];
	flatten = [flatten stringByReplacingOccurrencesOfString:@"\n" withString:@""];
	flatten = [flatten stringByReplacingOccurrencesOfString:@"\t" withString:@" "];
	NSError *error = nil;
	NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"  +" options:NSRegularExpressionCaseInsensitive error:&error];
	flatten = [regex stringByReplacingMatchesInString:flatten options:0 range:NSMakeRange(0, [flatten length]) withTemplate:@" "];
	return flatten;
}
- (NSString *)jsonString{
	NSError *error;
	NSArray *array = [self copy];
	BOOL isValidJSONObject = [NSJSONSerialization isValidJSONObject:self];
	if (isValidJSONObject) {
		NSData *jsonData = [NSJSONSerialization dataWithJSONObject:array options:0 error:&error];
		if (!jsonData) {
			DLog(@"error: %@", error.localizedDescription);
			return @"{}";
		}
		else {
			return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
		}
	}
	else {
		return @"Not a valid JSON object";
	}
}

- (void)logUndefinedKey:(NSString*)key{
    //DLog(@"UNDEFINED KEY: %@",key);
}

@end
