//
//  NSURLRequest.m
//  Homestay
//
//  Created by Liam Dunne on 22/07/2014.
//  Copyright (c) 2014 Homestay. All rights reserved.
//

#import "NSURLRequest+Extensions.h"
#import "NSObject+Extensions.h"
#import "NSString+Extensions.h"

@implementation NSURLRequest (Extensions)

- (void)logCurl{
    CurlLog(@"%@",[self stringForCurl]);
}
- (void)logData:(NSData *)data response:(NSURLResponse *)response error:(NSError *)error{
    CurlLog(@"%@",[self stringForData:data response:response error:error]);
}

- (NSString *)stringForCurl {
    __block NSString *log = [NSString stringWithFormat:@"curl -s -k"];
    NSString *authorization = [[self allHTTPHeaderFields] valueForKey:@"Authorization"];
    if (authorization){
        log = [log stringByAppendingFormat:@" -H \"%@: %@\"", @"Authorization", authorization];
    }
    if (![self.HTTPMethod isEqualToString:@"GET"]){
        log = [log stringByAppendingFormat:@" --request %@ ", self.HTTPMethod];
    }
    [[self allHTTPHeaderFields] enumerateKeysAndObjectsUsingBlock: ^(id key, id obj, BOOL *stop) {
        log = [log stringByAppendingFormat:@" -H \"%@: %@\"", key, obj];
    }];
    log = [log stringByAppendingFormat:@" \"%@\"", self.URL.absoluteString];
    NSString *data = [[NSString alloc] initWithData:self.HTTPBody encoding:NSUTF8StringEncoding];
    if (data && data.length) {
        data = [[data flatten] compactString];
        NSString *escapedData = data;//[data stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""];
        log = [log stringByAppendingFormat:@" --data '%@'", escapedData];
        
//        id jsonObject = [NSJSONSerialization JSONObjectWithData:self.HTTPBody options:NSJSONReadingAllowFragments error:nil];
//        log = [log stringByAppendingFormat:@"\nHTTPBody: %@",[[jsonObject jsonString] flatten]];
    }
    return log;
}
- (NSString*)stringForData:(NSData *)data response:(NSURLResponse *)response error:(NSError *)error {
    NSUInteger statusCode = [(NSHTTPURLResponse *)response statusCode];
    NSString *log = @"";
    log = [log stringByAppendingFormat:@"curl response %@[%@]: %@", [self HTTPMethod], @(statusCode), [self URL]];
//    if ([self allHTTPHeaderFields]) log = [log stringByAppendingFormat:@"[request allHTTPHeaderFields] = %@\n", [[self allHTTPHeaderFields] jsonString]];
//    if ([(NSHTTPURLResponse*)response allHeaderFields]) log = [log stringByAppendingFormat:@"[response allHeaderFields] = %@\n", [[(NSHTTPURLResponse*)response allHeaderFields] jsonString]];
    if (error) {
        log = [log stringByAppendingFormat:@"\ncurl: <ERROR: %@> ", [error localizedDescription]];
    }
    else {
        
        NSInteger responseCode = [(NSHTTPURLResponse *)response statusCode];
        NSError *responseError;
        
        id jsonObject = nil;
        if ([data length] != 0) {
            NSError *jsonError;
            jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonError];
            if (jsonError) {
                NSAssert(jsonError != nil, @"jsonError = \n%@", jsonError);
                if ([jsonError code] != 3840) {
                    NSDictionary *userInfo = @{ @"statusCode":@(jsonError.code), NSLocalizedDescriptionKey:jsonError.localizedDescription };
                    responseError = [NSError errorWithDomain:@"Homestay.com" code:responseCode userInfo:userInfo];
                    DLog(@"jsonError: %@",responseError);
                }
            }
            if ([jsonObject isKindOfClass:[NSDictionary class]]) {
                jsonObject = @[jsonObject];
            }
        }
        NSArray *array = (NSArray *)jsonObject;
        if (array) log = [log stringByAppendingFormat:@"\ncurl response: %@",[array jsonString]];
    }
    return log;
}

- (NSString *)log {
    NSString *log = @"";
    log = [log stringByAppendingFormat:@"[request URL]                 = %@\n", [self URL]];
    log = [log stringByAppendingFormat:@"[request HTTPMethod]          = %@\n", [self HTTPMethod]];
    log = [log stringByAppendingFormat:@"[request allHTTPHeaderFields] = %@\n", [[self allHTTPHeaderFields] flatten]];
    if ([self HTTPBody] && [[self HTTPBody] bytes]) {
        log = [log stringByAppendingFormat:@"[request HTTPBody]            = %@", [[NSString stringWithUTF8String:[[self HTTPBody] bytes]] flatten]];
    }
    return log;
}

- (NSDictionary*)dictionaryFromHTTPBody{
    NSString *httpBodyString = [[NSString alloc] initWithData:self.HTTPBody encoding:NSUTF8StringEncoding];
    if (httpBodyString && httpBodyString.length) {
        NSArray *array = [httpBodyString componentsSeparatedByString:@"&"];
        NSMutableDictionary *mutableDictionary = [NSMutableDictionary dictionary];
        [array enumerateObjectsUsingBlock:^(NSString *string, NSUInteger idx, BOOL *stop){
            NSArray *keyValue = [string componentsSeparatedByString:@"="];
            if ([keyValue count]==2){
                [mutableDictionary addEntriesFromDictionary:@{keyValue[0]:keyValue[1]}];
            } else {
                [mutableDictionary addEntriesFromDictionary:@{@"non_key_value_pair":string}];
            }
        }];
        return [mutableDictionary copy];
    }
    return nil;
}

@end
