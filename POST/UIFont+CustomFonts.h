//
//  UIFont+CustomFonts.h
//
//  Created by Liam Dunne on 02/01/2014.
//  Copyright (c) 2014 Lmd64. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (CustomFonts)

+ (UIFont*)customFontOfSize:(CGFloat)fontSize;
+ (UIFont*)customBoldFontOfSize:(CGFloat)fontSize;
+ (UIFont*)customPreferredFontForTextStyle:(NSString*)textStyle;

@end
