//
//  UIView+VisualDebug.m
//  Homestay
//
//  Created by Liam Dunne on 09/05/2014.
//  Copyright (c) 2014 Liam Dunne. All rights reserved.
//

#import "UIView+VisualDebug.h"

#define kBorderWidth 1.0
#define kBorderRadius 4.0

@implementation UIView (VisualDebug)
- (void)debugViewBorders {
    [self.layer setBorderColor:[[UIColor lightGrayColor] colorWithAlphaComponent:0.8].CGColor];
    [self.layer setBorderWidth:kBorderWidth];
    [self.layer setCornerRadius:kBorderRadius];
}
- (void)debugBorders {
    [self.layer setBorderColor:[[UIColor lightGrayColor] colorWithAlphaComponent:0.8].CGColor];
    [self.layer setBorderWidth:kBorderWidth];
    [self.layer setCornerRadius:kBorderRadius];
    [self debugSubviewBounds];
}
- (void)debugSubviewBounds {
    NSArray *subviews = [self subviews];
    [subviews enumerateObjectsUsingBlock: ^(UIView *view, NSUInteger idx, BOOL *stop) {
        CGFloat hue = (float)idx / (float)[subviews count];
        UIColor *_color = [UIColor colorWithHue:hue saturation:1.0 brightness:1.0 alpha:0.8];
        //[view.layer setBackgroundColor:[_color colorWithAlphaComponent:0.05].CGColor];
        [view.layer setBorderColor:_color.CGColor];
        [view.layer setBorderWidth:kBorderWidth];
        [view.layer setCornerRadius:kBorderRadius];
        [view debugSubviewBounds];
    }];
}

@end
