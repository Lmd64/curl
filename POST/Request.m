//
//  Request.m
//  curly
//
//  Created by Liam Dunne on 08/02/2015.
//  Copyright (c) 2015 lmd64. All rights reserved.
//

#import "Request.h"
#import "Format.h"


@implementation Request

@dynamic action;
@dynamic body;
@dynamic dateCreated;
@dynamic displayParsedOutput;
@dynamic endpoint;
@dynamic lastDateExecuted;
@dynamic method;
@dynamic successful;
@dynamic header;
@dynamic headerType;
@dynamic format;

@end
