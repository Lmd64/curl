//
//  RequestPanelViewController.h
//  curly
//
//  Created by Liam Dunne on 08/02/2015.
//  Copyright (c) 2015 lmd64. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Request+Helper.h"

@protocol RequestPanelDelegate <NSObject>
- (void)updateRequest;
- (void)performFetchRequest;
- (IBAction)didTapHeaderTypeButton:(id)sender;
@end

@interface RequestPanelViewController : UIViewController <UITextFieldDelegate>

@property (nonatomic,assign) id <RequestPanelDelegate> delegate;

@property (strong, nonatomic) Request *request;

@property (weak, nonatomic) IBOutlet UISegmentedControl *methodSegmentedControl;
@property (weak, nonatomic) IBOutlet UITextField *actionTextField;
@property (weak, nonatomic) IBOutlet UITextField *bodyTextField;
@property (weak, nonatomic) IBOutlet UIButton *headerTypeButton;
@property (weak, nonatomic) IBOutlet UITextField *headerTextField;

@property (weak, nonatomic) IBOutlet UILabel *title1Label;
@property (weak, nonatomic) IBOutlet UILabel *title2Label;
@property (weak, nonatomic) IBOutlet UILabel *title3Label;
@property (weak, nonatomic) IBOutlet UILabel *subtitle1Label;
@property (weak, nonatomic) IBOutlet UILabel *subtitle2Label;
@property (weak, nonatomic) IBOutlet UILabel *subtitle3Label;

@property (weak, nonatomic) IBOutlet UITextField *title1TextField;
@property (weak, nonatomic) IBOutlet UITextField *title2TextField;
@property (weak, nonatomic) IBOutlet UITextField *title3TextField;
@property (weak, nonatomic) IBOutlet UITextField *subtitle1TextField;
@property (weak, nonatomic) IBOutlet UITextField *subtitle2TextField;
@property (weak, nonatomic) IBOutlet UITextField *subtitle3TextField;

@property (nonatomic,assign) NSUInteger panelCount;

- (void)setHeaderTypeButtonTitle:(NSString*)title;

- (NSString*)method;

@end
