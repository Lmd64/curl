//
//  CoreDataManager.m
// CURL
//
//  Created by Liam Dunne on 06/04/2014.
//  Copyright (c) 2014 Lmd64. All rights reserved.
//

#import "CoreDataManager.h"

@interface CoreDataManager () {
	NSManagedObjectContext *_managedObjectContext;
	NSManagedObjectModel *_managedObjectModel;
	NSPersistentStoreCoordinator *_persistentStoreCoordinator;
}
@end

@implementation CoreDataManager

+ (CoreDataManager *)sharedInstance {
	static CoreDataManager *sharedInstance;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
	    sharedInstance = [[CoreDataManager alloc] init];
	});
	return sharedInstance;
}

// //////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Core Data stack
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext {
	if (_managedObjectContext != nil) {
		return _managedObjectContext;
	}

	NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
	if (coordinator != nil) {
		_managedObjectContext = [[NSManagedObjectContext alloc] init];
		[_managedObjectContext setPersistentStoreCoordinator:coordinator];
	}
	//_managedObjectContext.undoManager = [[NSUndoManager alloc] init];
	[_managedObjectContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
	return _managedObjectContext;
}

- (NSManagedObjectContext *)managedObjectContextForThread:(NSThread *)thread {
	NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
	NSManagedObjectContext *managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
	[managedObjectContext setPersistentStoreCoordinator:coordinator];
	[managedObjectContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
	return managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel {
	if (_managedObjectModel != nil) {
		return _managedObjectModel;
	}
	NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Model" withExtension:@"momd"];
	_managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
	return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
	if (_persistentStoreCoordinator != nil) {
		return _persistentStoreCoordinator;
	}

	NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Model.sqlite"];
	CLog(@"%@", [storeURL path]);

	NSError *error = nil;
	_persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];

	//* Performing automatic lightweight migration by passing the following dictionary as the options parameter:
	NSDictionary *options = @{ NSPersistentStoreUbiquitousContentNameKey: @"CURLPersistentStoreUbiquitousContentName",
		                       NSMigratePersistentStoresAutomaticallyOption:@YES,
		                       NSInferMappingModelAutomaticallyOption:@YES };
	if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
		/*
		   Replace this implementation with code to handle the error appropriately.

		   abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.

		   Typical reasons for an error here include:
		 * The persistent store is not accessible;
		 * The schema for the persistent store is incompatible with current managed object model.
		   Check the error message to determine what the actual problem was.


		   If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.

		   If you encounter schema incompatibility errors during development, you can reduce their frequency by:
		 * Simply deleting the existing store:
		   [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]

		 * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
		   [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption, [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];

		   Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.

		 */

//		//TODO: handle this better via automigration
//		DLog(@"### deleting persistent store at %@", storeURL);
//		[[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil];
//		if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
//			NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
//			abort();
//		}
	}

	return _persistentStoreCoordinator;
}

- (void)saveContext {
	NSError *error = nil;
	NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
	if (managedObjectContext != nil) {
		if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
			// Replace this implementation with code to handle the error appropriately.
			// abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
			NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
			abort();
		}
	}
}

// //////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Application's Documents directory
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory {
	return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

// //////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - iCloud Core Data
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)registerForiCloudNotifications {
	NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];

	[notificationCenter addObserver:self
	                       selector:@selector(storesWillChange:)
	                           name:NSPersistentStoreCoordinatorStoresWillChangeNotification
	                         object:self.persistentStoreCoordinator];

	[notificationCenter addObserver:self
	                       selector:@selector(storesDidChange:)
	                           name:NSPersistentStoreCoordinatorStoresDidChangeNotification
	                         object:self.persistentStoreCoordinator];

	[notificationCenter addObserver:self
	                       selector:@selector(persistentStoreDidImportUbiquitousContentChanges:)
	                           name:NSPersistentStoreDidImportUbiquitousContentChangesNotification
	                         object:self.persistentStoreCoordinator];
}

- (void)persistentStoreDidImportUbiquitousContentChanges:(NSNotification *)changeNotification {
	DLog(@"%@", changeNotification);
	NSManagedObjectContext *context = [self managedObjectContextForThread:[NSThread currentThread]];

	[context performBlock: ^{
	    [context mergeChangesFromContextDidSaveNotification:changeNotification];
	    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationReloadData object:changeNotification];
	}];
}

- (void)storesWillChange:(NSNotification *)notification {
	DLog(@"%@", notification);
	NSManagedObjectContext *context = [self managedObjectContextForThread:[NSThread currentThread]];

	[context performBlockAndWait: ^{
	    NSError *error;

	    if ([context hasChanges]) {
	        BOOL success = [context save:&error];

	        if (!success && error) {
	            // perform error handling
	            NSLog(@"%@", [error localizedDescription]);
			}
		}

	    [context reset];
	}];

	// Refresh your User Interface.
	[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationReloadData object:notification];
}

- (void)storesDidChange:(NSNotification *)notification {
	// Refresh your User Interface.
	[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationReloadData object:notification];
}

@end
