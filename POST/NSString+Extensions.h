//
//  NSString+MD5.h
//  Homestay
//
//  Created by Liam Dunne on 16/12/2013.
//  Copyright (c) 2013 Liam Dunne. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Extensions)

- (NSString *)MD5;
- (NSString*)urlEncoded;
- (NSString*)htmlEncoded;
- (NSString*)escapedString;
- (NSString*)escapedEmailAddress;
- (NSString *)compactString;
- (NSString *)trimWhitespace;
- (NSString*)sentenceCase;

- (BOOL)validateEmailAddress;
- (BOOL)validatePassword;
- (BOOL)validateMessageBodyContent;

- (NSDictionary*)parseQueryItems;

- (NSString*)stringByRemovingErrorCodeFromErrorMessage;
+ (NSString*)countryNameForCountryCode:(NSString*)countryCode;

@end
