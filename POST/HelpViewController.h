//
//  HelpViewController.h
//  curly
//
//  Created by Liam Dunne on 26/04/2014.
//  Copyright (c) 2014 Lmd64. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface HelpViewController : UIViewController <MFMailComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UITextView *helpTextView;
@property (weak, nonatomic) IBOutlet UIButton *emailDeveloperButton;
- (IBAction)emailDeveloperButtonTapped:(id)sender;
@end
