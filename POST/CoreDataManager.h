//
//  CoreDataManager.h
// CURL
//
//  Created by Liam Dunne on 06/04/2014.
//  Copyright (c) 2014 Lmd64. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
@class Request;
@class User;
@class Device;

@interface CoreDataManager : NSObject

+ (CoreDataManager *)sharedInstance;

- (NSManagedObjectContext *)managedObjectContext;
- (NSManagedObjectContext *)managedObjectContextForThread:(NSThread *)thread;
- (NSManagedObjectModel *)managedObjectModel;
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
- (void)registerForiCloudNotifications;

@end
