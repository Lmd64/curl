//
//  ViewController.h
// CURL
//
//  Created by Liam Dunne on 06/04/2014.
//  Copyright (c) 2014 Lmd64. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RequestPanelViewController.h"

@protocol RequestEditorDelegate <NSObject>
- (void)reloadData;
@end
@class Request;

typedef void (^CURLURLSessionResponseHandler)(NSData *data, NSURLResponse *response, NSError *error);

@interface RequestViewController : UIViewController
<UITextFieldDelegate, UINavigationControllerDelegate, UIScrollViewDelegate,
UITableViewDelegate, UITableViewDataSource,
UIActionSheetDelegate,RequestPanelDelegate,
NSURLSessionDataDelegate, NSURLSessionDelegate, NSURLSessionDownloadDelegate, NSURLSessionTaskDelegate>

@property (assign, nonatomic) id <RequestEditorDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIScrollView *headScrollView;
@property (weak, nonatomic) IBOutlet UIView *headContentView;

@property (weak, nonatomic) IBOutlet UIScrollView *bodyScrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *goBarButtonItem;

@property (strong, nonatomic) RequestPanelViewController *panelViewController;

//@property (weak, nonatomic) IBOutlet UISegmentedControl *methodSegmentedControl;
//@property (weak, nonatomic) IBOutlet UITextField *actionTextField;
//@property (weak, nonatomic) IBOutlet UITextField *bodyTextField;

@property (weak, nonatomic) IBOutlet UITextView *resultTextView;
@property (weak, nonatomic) IBOutlet UITableView *resultsTableView;

@property (weak, nonatomic) IBOutlet UIButton *CURLcopyButton;
@property (weak, nonatomic) IBOutlet UIButton *JSONcopyButton;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (weak, nonatomic) IBOutlet UISwitch *formatSwitch;
@property (weak, nonatomic) IBOutlet UILabel *formatSwitchLabel;

//@property (weak, nonatomic) IBOutlet UILabel *title1Label;
//@property (weak, nonatomic) IBOutlet UILabel *title2Label;
//@property (weak, nonatomic) IBOutlet UILabel *title3Label;
//@property (weak, nonatomic) IBOutlet UILabel *subtitle1Label;
//@property (weak, nonatomic) IBOutlet UILabel *subtitle2Label;
//@property (weak, nonatomic) IBOutlet UILabel *subtitle3Label;
//
//@property (weak, nonatomic) IBOutlet UITextField *title1TextField;
//@property (weak, nonatomic) IBOutlet UITextField *title2TextField;
//@property (weak, nonatomic) IBOutlet UITextField *title3TextField;
//@property (weak, nonatomic) IBOutlet UITextField *subtitle1TextField;
//@property (weak, nonatomic) IBOutlet UITextField *subtitle2TextField;
//@property (weak, nonatomic) IBOutlet UITextField *subtitle3TextField;

@property (strong, nonatomic) Request *request;

- (IBAction)goButtonTapped:(id)sender;
- (IBAction)CURLcopyButtonTapped:(id)sender;
- (IBAction)JSONcopyButtonTapped:(id)sender;
- (IBAction)shareButtonTapped:(id)sender;

@end
