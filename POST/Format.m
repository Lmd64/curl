//
//  Format.m
//  curl
//
//  Created by Liam Dunne on 08/04/2014.
//  Copyright (c) 2014 Lmd64. All rights reserved.
//

#import "Format.h"
#import "Request.h"


@implementation Format

@dynamic title1;
@dynamic title2;
@dynamic title3;
@dynamic subtitle1;
@dynamic subtitle2;
@dynamic subtitle3;
@dynamic request;

@end
