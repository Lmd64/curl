//
//  NSURLRequest.h
//  Homestay
//
//  Created by Liam Dunne on 22/07/2014.
//  Copyright (c) 2014 Homestay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSURLRequest (Extensions)

- (void)logCurl;
- (void)logData:(NSData *)data response:(NSURLResponse *)response error:(NSError *)error;
- (NSString *)stringForCurl;
- (NSString*)stringForData:(NSData *)data response:(NSURLResponse *)response error:(NSError *)error;
- (NSDictionary*)dictionaryFromHTTPBody;

//- (NSString *)log;


@end
