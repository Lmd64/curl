//
//  Constants.h
// CURL
//
//  Created by Liam Dunne on 06/04/2014.
//  Copyright (c) 2014 Lmd64. All rights reserved.
//

#ifndef POST_Constants_h
#define POST_Constants_h

#define kColorDefault [UIColor colorWithRed:131.0 / 255.0 green:138.0 / 255.0 blue:154.0 / 255.0 alpha:1.0]

#define kNotificationReloadData @"kNotificationReloadData"
#define kEndpoint @"kEndpoint"
#define kFirstTimeUseAnimationOnRequestViewController @"firstTimeUseAnimationOnRequestViewController"

#endif
