//
//  UIFont+CustomFonts.m
//
//  Created by Liam Dunne on 02/01/2014.
//  Copyright (c) 2014 Lmd64. All rights reserved.
//

#import "NSObject+MethodExchange.h"
#import "UIFont+CustomFonts.h"

#define EXCHANGE_METHOD(a, b) [[self class] exchangeMethod : @selector(a) withNewMethod : @selector(b)]

@implementation UIFont (CustomFonts)

+ (void)load {
	[self logAllCustomFonts];
	[[self class] exchangeMethod:@selector(systemFontOfSize:) withNewMethod:@selector(customFontOfSize:)];
	[[self class] exchangeMethod:@selector(boldSystemFontOfSize:) withNewMethod:@selector(customBoldFontOfSize:)];
}

+ (void)logAllCustomFonts {
	DLog(@"#########################");
	DLog(@"systemFontOfSize      :%@", [UIFont systemFontOfSize:24.0]);
	DLog(@"boldSystemFontOfSize  :%@", [UIFont boldSystemFontOfSize:24.0]);
	DLog(@"#########################");
}

+ (UIFont *)customFontOfSize:(CGFloat)fontSize {
    return [UIFont fontWithName:@"Inconsolata" size:fontSize];
	//return [UIFont fontWithName:@"PTMono-Regular" size:fontSize];
}

+ (UIFont *)customBoldFontOfSize:(CGFloat)fontSize {
    return [UIFont fontWithName:@"Inconsolata" size:fontSize];
	//return [UIFont fontWithName:@"PTMono-Bold" size:fontSize];
}

+ (UIFont *)customPreferredFontForTextStyle:(NSString *)textStyle {
	UIFont *preferredFontForTextStyle = [UIFont preferredFontForTextStyle:textStyle];
    return [UIFont fontWithName:@"Inconsolata" size:preferredFontForTextStyle.pointSize];
	//return [UIFont fontWithName:@"PTMono-Regular" size:preferredFontForTextStyle.pointSize];
}

@end
