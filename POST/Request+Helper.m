//
//  Request+Helper.m
// CURL
//
//  Created by Liam Dunne on 06/04/2014.
//  Copyright (c) 2014 Lmd64. All rights reserved.
//

#import "Request+Helper.h"
#import "Format.h"
#import "CoreDataManager.h"

@implementation Request (Helper)

+ (void)load {
	if (![[NSUserDefaults standardUserDefaults] valueForKeyPath:@"endpoint"]) [[NSUserDefaults standardUserDefaults] setValue:kDefaultEndpoint forKeyPath:@"endpoint"];
	if (![[NSUserDefaults standardUserDefaults] valueForKeyPath:@"action"]) [[NSUserDefaults standardUserDefaults] setValue:kDefaultAction forKeyPath:@"action"];
	if (![[NSUserDefaults standardUserDefaults] valueForKeyPath:@"method"]) [[NSUserDefaults standardUserDefaults] setValue:kDefaultMethod forKeyPath:@"method"];

	if (![[NSUserDefaults standardUserDefaults] valueForKeyPath:@"title1"]) [[NSUserDefaults standardUserDefaults] setValue:@"" forKeyPath:@"title1"];
	if (![[NSUserDefaults standardUserDefaults] valueForKeyPath:@"title2"]) [[NSUserDefaults standardUserDefaults] setValue:@"" forKeyPath:@"title2"];
	if (![[NSUserDefaults standardUserDefaults] valueForKeyPath:@"title3"]) [[NSUserDefaults standardUserDefaults] setValue:@"" forKeyPath:@"title3"];
	if (![[NSUserDefaults standardUserDefaults] valueForKeyPath:@"subtitle1"]) [[NSUserDefaults standardUserDefaults] setValue:@"" forKeyPath:@"subtitle1"];
	if (![[NSUserDefaults standardUserDefaults] valueForKeyPath:@"subtitle2"]) [[NSUserDefaults standardUserDefaults] setValue:@"" forKeyPath:@"subtitle2"];
	if (![[NSUserDefaults standardUserDefaults] valueForKeyPath:@"subtitle3"]) [[NSUserDefaults standardUserDefaults] setValue:@"" forKeyPath:@"subtitle3"];
    
	[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (Request *)new {
	NSDictionary *dictionary = @{ @"endpoint":[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"endpoint"],
		                          @"action":[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"action"],
		                          @"method":[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"method"],
		                          @"dateCreated":[NSDate date],
		                          @"title1":[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"title1"],
		                          @"title2":[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"title2"],
		                          @"title3":[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"title3"],
		                          @"subtitle1":[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"subtitle1"],
		                          @"subtitle2":[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"subtitle2"],
		                          @"subtitle3":[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"subtitle3"] };
	return [Request requestWithDictionary:dictionary];
}

+ (Request *)requestWithDictionary:(NSDictionary *)dictionary {
	NSManagedObjectContext *context = [[CoreDataManager sharedInstance] managedObjectContext];
	NSEntityDescription *entityDescription = [NSEntityDescription entityForName:NSStringFromClass([self class]) inManagedObjectContext:context];
	Request *request = [[Request alloc] initWithEntity:entityDescription insertIntoManagedObjectContext:context];

	if (dictionary[@"endpoint"]) request.endpoint = dictionary[@"endpoint"];
	if (dictionary[@"action"]) request.action = dictionary[@"action"];
	if (dictionary[@"method"]) request.method = dictionary[@"method"];
	if (dictionary[@"dateCreated"]) request.dateCreated = dictionary[@"dateCreated"];
	if (dictionary[@"lastDateExecuted"]) request.lastDateExecuted = dictionary[@"lastDateExecuted"];
	if (dictionary[@"successful"]) request.successful = dictionary[@"successful"];

	[request updateFormat];
	if (dictionary[@"title1"]) request.format.title1 = dictionary[@"title1"];
	if (dictionary[@"title2"]) request.format.title2 = dictionary[@"title2"];
	if (dictionary[@"title3"]) request.format.title3 = dictionary[@"title3"];
	if (dictionary[@"subtitle1"]) request.format.subtitle1 = dictionary[@"subtitle1"];
	if (dictionary[@"subtitle2"]) request.format.subtitle2 = dictionary[@"subtitle2"];
	if (dictionary[@"subtitle3"]) request.format.subtitle3 = dictionary[@"subtitle3"];

	NSError *error;
	BOOL didSave = [context save:&error];
	if (!didSave) {
		DLog(@"Whoops! CoreData: : %@\n%@", [error localizedDescription], request);
	}

	return request;
}

- (void)updateFormat {
	if (!self.format) {
		NSManagedObjectContext *context = [[CoreDataManager sharedInstance] managedObjectContext];
		NSEntityDescription *entityDescription = [NSEntityDescription entityForName:NSStringFromClass([Format class]) inManagedObjectContext:context];
		Format *format = [[Format alloc] initWithEntity:entityDescription insertIntoManagedObjectContext:context];
		format.request = self;
		self.format = format;
	}
}

+ (NSFetchedResultsController *)resultsController {
	NSManagedObjectContext *context = [[CoreDataManager sharedInstance] managedObjectContext];
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([self class])];
	NSPredicate *predicate = nil;
	[fetchRequest setPredicate:predicate];
	NSArray *sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"method" ascending:NO],
	                             [NSSortDescriptor sortDescriptorWithKey:@"dateCreated" ascending:NO]];
	[fetchRequest setSortDescriptors:sortDescriptors];
	NSFetchedResultsController *fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:context sectionNameKeyPath:@"method" cacheName:nil];
	NSError *error;
	[fetchedResultsController performFetch:&error];
	return fetchedResultsController;
}

- (void)save {
	NSManagedObjectContext *context = [[CoreDataManager sharedInstance] managedObjectContext];
	NSError *error;
	BOOL didSave = [context save:&error];
	if (!didSave) {
		DLog(@"Whoops! CoreData: : %@\n%@", [error localizedDescription], self);
	}
}

- (void)delete {
	NSManagedObjectContext *context = [[CoreDataManager sharedInstance] managedObjectContext];
	[context deleteObject:self.format];
	[context deleteObject:self];
	NSError *error;
	if (![context save:&error]) {
		DLog(@"Whoops, couldn't delete: %@", [error localizedDescription]);
	}
}

- (NSString *)description {
	NSString *description = @"";
	description = [description stringByAppendingFormat:@"endpoint:%@, ", self.endpoint];
	description = [description stringByAppendingFormat:@"action:%@, ", self.action];
	description = [description stringByAppendingFormat:@"method:%@, ", self.method];
	description = [description stringByAppendingFormat:@"dateCreated:%@, ", self.dateCreated];
    description = [description stringByAppendingFormat:@"lastDateExecuted:%@, ", self.lastDateExecuted];
    description = [description stringByAppendingFormat:@"successful:%@\n", self.successful];
    description = [description stringByAppendingFormat:@"headerType:%@, ", self.headerType];
    description = [description stringByAppendingFormat:@"header:%@, ", self.header];
	return description;
}

- (NSAttributedString *)attributedString {
	NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] init];
	NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
	paragraphStyle.lineBreakMode = NSLineBreakByClipping;
	[paragraphStyle setAlignment:NSTextAlignmentLeft];

	NSString *string;
	UIFont *font;
	NSDictionary *attributes;
	NSAttributedString *attributedStringFragment;

	if (self.method) {
		string = self.method;
		if (self.method && self.action) string = [string stringByAppendingString:@":"];
		font = [UIFont boldSystemFontOfSize:18.0];
		attributes = @{ NSFontAttributeName:font, NSForegroundColorAttributeName:[UIColor lightGrayColor], NSParagraphStyleAttributeName:paragraphStyle };
		attributedStringFragment = [[NSMutableAttributedString alloc] initWithString:string attributes:attributes];
		[attributedString appendAttributedString:attributedStringFragment];
	}

	if (self.action) {
		string = self.action;
		font = [UIFont systemFontOfSize:18.0];
		attributes = @{ NSFontAttributeName:font, NSForegroundColorAttributeName:[UIColor darkGrayColor], NSParagraphStyleAttributeName:paragraphStyle };
		attributedStringFragment = [[NSMutableAttributedString alloc] initWithString:string attributes:attributes];
		[attributedString appendAttributedString:attributedStringFragment];
	}

	return attributedString;
}

- (NSURL *)urlForRequest {
	NSString *urlString = [NSString stringWithFormat:@"%@%@", self.endpoint, self.action];
	return [NSURL URLWithString:urlString];
}

- (NSURLRequest *)URLRequest {
	NSURL *URL = [self urlForRequest];
	NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:URL];
	[urlRequest setHTTPMethod:self.method];
	return [urlRequest copy];
}

- (NSString *)curlRequest {
	NSURLRequest *request = [self URLRequest];
	__block NSString *curlstr = [NSString stringWithFormat:@"curl --verbose --insecure --request %@ ", request.HTTPMethod];
	curlstr = [curlstr stringByAppendingFormat:@" %@", request.URL.absoluteString];
	[[request allHTTPHeaderFields] enumerateKeysAndObjectsUsingBlock: ^(id key, id obj, BOOL *stop) {
	    curlstr = [curlstr stringByAppendingFormat:@" --header \"%@: %@\"", key, obj];
	}];
	NSString *data = [[NSString alloc] initWithData:request.HTTPBody encoding:NSUTF8StringEncoding];
	if (data && data.length) {
		NSString *escapedData = [data stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""];
		curlstr = [curlstr stringByAppendingFormat:@" --data \"%@\"", escapedData];
	}
	return curlstr;
}

- (UIImage *)imageForStatus:(CGSize)size {
	CGRect frame = CGRectMake(0, 0, size.width, size.height);
	UIView *view = [[UIView alloc] initWithFrame:frame];
	[view setBackgroundColor:[UIColor whiteColor]];

	NSString *text = @"";
	UIColor *color = nil;
	if ([self successful] == nil) {
	}
	else if ([[self successful]  boolValue] == YES) {
		text = @"✓";
		color = [UIColor colorWithHue:0.4 saturation:0.9 brightness:0.6 alpha:0.50];
	}
	else if ([[self successful]  boolValue] == NO) {
		text = @"✕";
		color = [UIColor colorWithRed:0.84 green:0.35 blue:0.28 alpha:1.0];
	}
	else {
		text = @"";
		color = nil;
	}

	CGRect labelFrame = CGRectInset(frame, CGRectGetWidth(frame) * 0.1, CGRectGetHeight(frame) * 0.1);
	UILabel *label = [[UILabel alloc] initWithFrame:labelFrame];
	[label setBackgroundColor:[UIColor clearColor]];
	[label setText:text];
	[label setTextAlignment:NSTextAlignmentCenter];
	[label setFont:[UIFont fontWithName:@"Apple SD Gothic Neo" size:24.0]];
	[label setMinimumScaleFactor:6.0];
	[label setTextColor:color];
	[label setClipsToBounds:NO];
	[view addSubview:label];

	UIGraphicsBeginImageContextWithOptions(view.bounds.size, NO, 0.0);
	[view.layer renderInContext:UIGraphicsGetCurrentContext()];
	UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();

	return image;
}

- (BOOL)hasSubtitle {
	if (self.format.subtitle1 || self.format.subtitle2 || self.format.subtitle3) {
		return YES;
	}
	else {
		return NO;
	}
}

@end
