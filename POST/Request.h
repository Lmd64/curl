//
//  Request.h
//  curly
//
//  Created by Liam Dunne on 08/02/2015.
//  Copyright (c) 2015 lmd64. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Format;

@interface Request : NSManagedObject

@property (nonatomic, retain) NSString * action;
@property (nonatomic, retain) NSString * body;
@property (nonatomic, retain) NSDate * dateCreated;
@property (nonatomic, retain) NSNumber * displayParsedOutput;
@property (nonatomic, retain) NSString * endpoint;
@property (nonatomic, retain) NSDate * lastDateExecuted;
@property (nonatomic, retain) NSString * method;
@property (nonatomic, retain) NSNumber * successful;
@property (nonatomic, retain) NSString * header;
@property (nonatomic, retain) NSString * headerType;
@property (nonatomic, retain) Format *format;

@end
