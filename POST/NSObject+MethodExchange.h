//
//  NSObject+MethodExchange.h
//
//  Created by Liam Dunne on 02/01/2014.
//  Copyright (c) 2014 Lmd64. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (MethodExchange)
+ (void)exchangeMethod:(SEL)origSel withNewMethod:(SEL)newSel;
@end
