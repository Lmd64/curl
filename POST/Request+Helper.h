//
//  Request+Helper.h
// CURL
//
//  Created by Liam Dunne on 06/04/2014.
//  Copyright (c) 2014 Lmd64. All rights reserved.
//

#import "Request.h"

#define kDefaultEndpoint         @"http://your.endpoint.com/v1/"
#define kDefaultAction           @"action"
#define kDefaultMethod           @"GET"
#define kDefaultDateCreated      [NSDate date]
#define kDefaultLastDateExecuted [NSDate date]
#define kDefaultSuccessful       @NO

@interface Request (Helper)

+ (Request *)new;
+ (Request *)requestWithDictionary:(NSDictionary *)dictionary;
+ (NSFetchedResultsController *)resultsController;

- (void)save;
- (void)delete;
- (void)updateFormat;
- (NSAttributedString *)attributedString;

- (NSURL *)urlForRequest;
- (NSURLRequest *)URLRequest;
- (NSString *)curlRequest;

- (UIImage *)imageForStatus:(CGSize)size;

- (BOOL)hasSubtitle;

@end
