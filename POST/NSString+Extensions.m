//
//  NSString+MD5.m
//  Homestay
//
//  Created by Liam Dunne on 16/12/2013.
//  Copyright (c) 2013 Liam Dunne. All rights reserved.
//

#import <CommonCrypto/CommonDigest.h>
#import "NSString+Extensions.h"
#import "NSObject+Extensions.h"

@implementation NSString (Extensions)

- (NSString*)MD5{
    // Create pointer to the string as UTF8
    const char *ptr = [self UTF8String];
    
    // Create byte array of unsigned chars
    unsigned char md5Buffer[CC_MD5_DIGEST_LENGTH];
    
    // Create 16 byte MD5 hash value, store in buffer
    CC_MD5(ptr, (CC_LONG)strlen(ptr), md5Buffer);
    
    // Convert MD5 value in the buffer to NSString of hex values
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x",md5Buffer[i]];
    
    return output;
}

- (NSString*)urlEncoded{
    NSString* urlEncoded = [self stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet];
    return urlEncoded;
}

- (NSString*)htmlEncoded{
    NSString* htmlEncoded = [self stringByRemovingPercentEncoding];
    return htmlEncoded;
}

- (NSString*)escapedString{
    NSString* escaped_value = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                                                                    (CFStringRef)self,
                                                                                                    NULL,
                                                                                                    (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                                    kCFStringEncodingUTF8));
    return escaped_value;
}

- (NSString*)escapedEmailAddress{
    NSString* escaped_value = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                                                                    (CFStringRef)self,
                                                                                                    NULL,
                                                                                                    (CFStringRef)@"!*'();:&=+$,/?%#[]",
                                                                                                    kCFStringEncodingUTF8));
    return escaped_value;
}

- (NSString *)compactString {
	NSError *error = nil;
	NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"  +" options:NSRegularExpressionCaseInsensitive error:&error];
	NSString *compactString = [regex stringByReplacingMatchesInString:self options:0 range:NSMakeRange(0, [self length]) withTemplate:@" "];
	compactString = [compactString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	return compactString;
}

- (NSString *)trimWhitespace {
	return [self stringByTrimmingCharactersInSet:[ NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (NSString*)sentenceCase{
    if (self.length){
        NSString *string = [NSString stringWithFormat:@"%@%@",[[self substringToIndex:1] uppercaseString],[self substringFromIndex:1] ];
        return string;
    } else {
        return @"";
    }
}

- (BOOL)validateEmailAddress{
    NSString *strictFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxFilterString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSPredicate *strictEmailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", strictFilterString];
    NSPredicate *laxEmailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", laxFilterString];
    BOOL strictFilterValidate = [strictEmailTest evaluateWithObject:self];
    BOOL laxFilterValidate = [laxEmailTest evaluateWithObject:self];
    return (strictFilterValidate || laxFilterValidate);
}

- (BOOL)validatePassword{
    return YES;
}

- (BOOL)validateMessageBodyContent{
    NSError *error = nil;
    NSString *output = [NSString stringWithFormat:@"checking matches in string '%@'",self];
    NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:
                                NSTextCheckingTypeLink|
                                NSTextCheckingTypePhoneNumber|
                                NSTextCheckingTypeAddress
                                                               error:&error];
    NSMutableArray *matches = [NSMutableArray array];
    [detector enumerateMatchesInString:self
                               options:kNilOptions
                                 range:NSMakeRange(0, [self length])
                            usingBlock:
     ^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
         NSString *matchString = nil;
         switch ([result resultType]) {
             case NSTextCheckingTypeLink: {
                 NSString *urlString = [[result URL] absoluteString];
                 if ([urlString rangeOfString:@"@"].location!=NSNotFound){
                     matchString = urlString;
                 }
                 NSInteger prefixSize = 8;
                 NSInteger loc = [result range].location-prefixSize;
                 NSInteger location = MAX(0,loc);
                 NSInteger length = prefixSize + (loc<0?loc:0);
                 NSRange matchingRange = NSMakeRange(location, length);
                 NSString *urlPrefix = [self substringWithRange:matchingRange];
                 urlString = [self substringWithRange:[result range]];
                 if ([urlPrefix rangeOfString:@"@"].location!=NSNotFound || [urlPrefix rangeOfString:@"AT"].location!=NSNotFound){
                     urlString = [NSString stringWithFormat:@"%@%@",urlPrefix,urlString];
                     matchString = urlString;
                 }
                 
             } break;
                 
             case NSTextCheckingTypePhoneNumber:
                 matchString = [result phoneNumber];
                 break;
                 
             case NSTextCheckingTypeAddress:
                 matchString = [[[result addressComponents] allValues] flatten];
                 break;
                 
             default:
                 break;
         }
         if (matchString){
             matchString = [NSString stringWithFormat:@"<%@>",matchString];
             [matches addObject:matchString];
         }
     }];
    
    NSString *didMatchString = @"";
    NSString *matchesString = @"";
    BOOL didFindMatchingContent = (BOOL)[matches count];
    if (didFindMatchingContent){
        didMatchString = @"<MATCH FOUND>";
        matchesString = [matches componentsJoinedByString:@", "];
        CLog(@"%@%@: %@", didMatchString,output,matchesString);
   }
    return didFindMatchingContent;
 
}

- (NSDictionary*)parseQueryItems{
    NSString *urlString = self;
    NSURLComponents *components = [[NSURLComponents alloc] initWithString:urlString];
    
//    if ([components respondsToSelector:@selector(queryItems)]){
//        NSArray *array = [components performSelector:@selector(queryItems)];
//        NSMutableDictionary *mutableDictionary = [NSMutableDictionary dictionary];
//        [array enumerateObjectsUsingBlock:^(NSURLQueryItem *item, NSUInteger idx, BOOL *stop){
//            if (item.value){
//                [mutableDictionary addEntriesFromDictionary:@{item.name:item.value}];
//            } else {
//                [mutableDictionary addEntriesFromDictionary:@{item.name:@"<NULL>"}];
//            }
//        }];
//        return [mutableDictionary copy];
//    } else  {
        NSArray *array = [components.query componentsSeparatedByString:@"&"];
        NSMutableDictionary *mutableDictionary = [NSMutableDictionary dictionary];
        [array enumerateObjectsUsingBlock:^(NSString *string, NSUInteger idx, BOOL *stop){
            NSArray *keyValue = [string componentsSeparatedByString:@"="];
            if ([keyValue count]==2){
                [mutableDictionary addEntriesFromDictionary:@{keyValue[0]:keyValue[1]}];
            } else {
                [mutableDictionary addEntriesFromDictionary:@{@"non_key_value_pair":string}];
            }
        }];
        return [mutableDictionary copy];
//    }
    
}

- (NSString*)stringByRemovingErrorCodeFromErrorMessage{
    NSRange range = [self rangeOfString:@" ("];
    if (range.location != NSNotFound) {
        NSInteger index = MIN(range.location, self.length-1);
        NSString *newString = [self substringToIndex:index];
        return newString;
    }
    return self;
}

+ (NSString*)countryNameForCountryCode:(NSString*)countryCode{
    NSLocale *locale = [NSLocale currentLocale];
    NSString *country = [locale displayNameForKey: NSLocaleCountryCode value: countryCode];
    return country;
}

@end
