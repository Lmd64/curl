//
//  HelpViewController.m
//  curly
//
//  Created by Liam Dunne on 26/04/2014.
//  Copyright (c) 2014 Lmd64. All rights reserved.
//

#import "HelpViewController.h"

@interface HelpViewController ()

@end

@implementation HelpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view.

	self.helpTextView.attributedText = [self attributedString];

	[self.emailDeveloperButton.titleLabel setFont:[UIFont boldSystemFontOfSize:self.emailDeveloperButton.titleLabel.font.pointSize]];

	[self.emailDeveloperButton setTitleColor:[UIColor colorWithWhite:1.0 alpha:1.0] forState:UIControlStateNormal];
	[self.emailDeveloperButton setTitleColor:[UIColor colorWithWhite:1.0 alpha:0.5] forState:UIControlStateHighlighted];
	[self.emailDeveloperButton setBackgroundColor:kColorDefault];
	[self.emailDeveloperButton.layer setCornerRadius:5.0];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (NSAttributedString *)attributedString {
	CGFloat fontScale = 1.0;
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
		fontScale = 1.4;
	}
	NSArray *fragments = @[@{ @"string":@"Curly\n", @"font":[UIFont boldSystemFontOfSize:fontScale * 30] },
	                       @{ @"string":@"a ", @"font":[UIFont systemFontOfSize:fontScale * 10] },
	                       @{ @"string":@"simple REST client\n", @"font":[UIFont boldSystemFontOfSize:fontScale * 10] },
	                       @{ @"string":@"for iOS, both for iPhone & iPad with:\n", @"font":[UIFont systemFontOfSize:fontScale * 10] },
	                       @{ @"string":@"\n", @"font":[UIFont systemFontOfSize:fontScale * 10] },
	                       @{ @"string":@"Minimal UI\n", @"font":[UIFont boldSystemFontOfSize:fontScale * 15] },
	                       @{ @"string":@"\n", @"font":[UIFont systemFontOfSize:fontScale * 10] },
	                       @{ @"string":@"Human-readable output\n", @"font":[UIFont boldSystemFontOfSize:fontScale * 15] },
	                       @{ @"string":@"Only see what you really need to see\n", @"font":[UIFont systemFontOfSize:fontScale * 10] },
	                       @{ @"string":@"Additionally, use KVC Collection Operators, such as ", @"font":[UIFont systemFontOfSize:fontScale * 10] },
	                       @{ @"string":@"@count\n", @"font":[UIFont boldSystemFontOfSize:fontScale * 10] },
	                       @{ @"string":@"\n", @"font":[UIFont systemFontOfSize:fontScale * 10] },
	                       @{ @"string":@"Easy endpoint switching\n", @"font":[UIFont boldSystemFontOfSize:fontScale * 15] },
	                       @{ @"string":@"One single place to point to a different API endpoint\n", @"font":[UIFont systemFontOfSize:fontScale * 10] },
	                       @{ @"string":@"\n", @"font":[UIFont systemFontOfSize:fontScale * 10] },
	                       @{ @"string":@"Clipboard actions\n", @"font":[UIFont boldSystemFontOfSize:fontScale * 15] },
	                       @{ @"string":@"curl", @"font":[UIFont boldSystemFontOfSize:fontScale * 10] },
	                       @{ @"string":@" the commandline\n", @"font":[UIFont systemFontOfSize:fontScale * 10] },
	                       @{ @"string":@"pretty-print JSON", @"font":[UIFont boldSystemFontOfSize:fontScale * 10] },
	                       @{ @"string":@"output\n", @"font":[UIFont systemFontOfSize:fontScale * 10] },
	                       @{ @"string":@"\n", @"font":[UIFont systemFontOfSize:fontScale * 10] },
	                       @{ @"string":@"iCloud CoreData sync\n", @"font":[UIFont boldSystemFontOfSize:fontScale * 15] },
	                       @{ @"string":@"\n", @"font":[UIFont systemFontOfSize:fontScale * 10] },
	                       @{ @"string":@"Other Features:\n", @"font":[UIFont boldSystemFontOfSize:fontScale * 15] },
	                       @{ @"string":@"Support for ", @"font":[UIFont systemFontOfSize:fontScale * 10] },
	                       @{ @"string":@"GET/PUT/POST/DELETE\n", @"font":[UIFont boldSystemFontOfSize:fontScale * 10] },
	                       @{ @"string":@"Count of items returned automatically\n", @"font":[UIFont systemFontOfSize:fontScale * 10] },
	                       @{ @"string":@"Tap a row to ", @"font":[UIFont systemFontOfSize:fontScale * 10] },
	                       @{ @"string":@"copy primary value\n", @"font":[UIFont boldSystemFontOfSize:fontScale * 10] },
	                       @{ @"string":@"New requests created based on last opened request\n", @"font":[UIFont systemFontOfSize:fontScale * 10] },
	    ];

	__block NSDictionary *attributes;

	NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
	paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
	[paragraphStyle setAlignment:NSTextAlignmentLeft];

	__block NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:@"" attributes:attributes];

	[fragments enumerateObjectsUsingBlock: ^(NSDictionary *dictionary, NSUInteger idx, BOOL *stop) {
	    NSString *string = dictionary[@"string"];
	    UIFont *font = dictionary[@"font"];
	    if (font) {
	        attributes = @{ NSFontAttributeName:font, NSForegroundColorAttributeName:[UIColor grayColor], NSParagraphStyleAttributeName:paragraphStyle };
		}
	    NSAttributedString *attributedStringFragment = [[NSMutableAttributedString alloc] initWithString:string attributes:attributes];
	    [attributedString appendAttributedString:attributedStringFragment];
	}];

	return attributedString;
}

// //////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - email delegate
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (IBAction)emailDeveloperButtonTapped:(id)sender {
	MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
	controller.mailComposeDelegate = self;
	[controller setToRecipients:@[@"curly_iOSsupport@gmail.com"]];
	[controller setSubject:@"Curly!"];
	[controller setMessageBody:@"Hello.\nRegarding your app Curly, I have a question:\n" isHTML:YES];
	if (controller) {
		[self presentViewController:controller animated:YES completion: ^{}];
	}
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
	NSString *message = @"";
	switch (result) {
		case MFMailComposeResultSent: message = NSLocalizedString(@"Email sent!", nil); break;

		case MFMailComposeResultFailed: message = NSLocalizedString(@"Email failed to send!", nil); break;

		case MFMailComposeResultCancelled: message = NSLocalizedString(@"Email cancelled!", nil); break;

		default:
			break;
	}
	DLog(@"%@", message);
	[self dismissViewControllerAnimated:YES completion: ^{}];
}

@end
