//
//  NSString+KeyValues.m
//  curl
//
//  Created by Liam Dunne on 23/04/2014.
//  Copyright (c) 2014 Lmd64. All rights reserved.
//

#import "NSString+KeyValues.h"

@implementation NSObject (Flat)

- (NSString *)flatten {
	NSString *flatten = [self description];
	flatten = [flatten stringByReplacingOccurrencesOfString:@"\n" withString:@""];
	flatten = [flatten stringByReplacingOccurrencesOfString:@"\t" withString:@" "];
	return flatten;
}

- (NSString *)jsonStringWithPrettyPrint:(BOOL)prettyPrint {
	NSError *error;
	NSArray *array = [self copy];
	BOOL isValidJSONObject = [NSJSONSerialization isValidJSONObject:self];
	if (isValidJSONObject) {
		NSData *jsonData = [NSJSONSerialization dataWithJSONObject:array options:0 error:nil];
		if (!jsonData) {
			NSLog(@"jsonStringWithPrettyPrint: error: %@", error.localizedDescription);
			return @"{}";
		}
		else {
			return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
		}
	}
	else {
		return nil;
	}
}

- (NSString *)stringFromObject {
	id value = self;
	if ([value isKindOfClass:[NSString class]]) {
	}
	else if ([value isKindOfClass:[NSArray class]]) {
		if ([value count]) {
			value = [[value firstObject] flatten];
		}
		else {
			return nil;
		}
	}
	else if ([value isKindOfClass:[NSDictionary class]]) {
		value = [value flatten];
	}
	else {
		return nil;
	}
	if (!value) return nil;
	return value;
}

- (id)safeValueForKeyPath:(NSString *)keyPath {
//	if ([keyPath validKeyPathForDictionary:(NSDictionary *)self]) {
	@try {
		return [self valueForKeyPath:keyPath];
	}
	@catch (NSException *e)
	{
		return nil;
	}
//	}
}

@end



@implementation NSString (KeyValues)

- (NSString *)compactString {
	NSError *error = nil;
	NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"  +" options:NSRegularExpressionCaseInsensitive error:&error];
	NSString *compactString = [regex stringByReplacingMatchesInString:self options:0 range:NSMakeRange(0, [self length]) withTemplate:@" "];
	compactString = [compactString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	return compactString;
}

- (BOOL)validKeyPathForDictionary:(NSDictionary *)dictionary {
	if ([self isEqualToString:@""])
		return NO;
	if ([[self substringFromIndex:self.length - 1] isEqualToString:@"."])
		return NO;
	if ([self rangeOfString:@"."].location != NSNotFound && [self rangeOfString:@"@"].location == NSNotFound)
		return YES;
	else if (![[dictionary allKeys] containsObject:self]) {
		NSArray *components = [self componentsSeparatedByString:@".@"];
		if ([self rangeOfString:@".@"].location != NSNotFound && [components count] != 2) {
			return NO;
		}
		else {
			NSArray *allowedCollectionOperators = @[@"sum", @"count", @"avg", @"max", @"min"];
			NSString *operator = [components lastObject];
			BOOL isAllowedCollectionOperators = [allowedCollectionOperators containsObject:operator];
			if (!isAllowedCollectionOperators)
				return NO;
		}
	}
	return YES;
}

- (NSString *)keyPath {
	NSArray *components = [self componentsSeparatedByString:@".@"];
	if ([self rangeOfString:@".@"].location != NSNotFound && [components count] != 2) {
		return self;
	}
	else {
		NSString *keypath = [components firstObject];
		return keypath;
	}
	return self;
}

- (NSAttributedString *)attributedStringWithColor:(UIColor *)color {
	NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
	paragraphStyle.lineBreakMode = NSLineBreakByClipping;
	[paragraphStyle setAlignment:NSTextAlignmentLeft];
    
	NSString *jsonString = [self jsonStringWithPrettyPrint:NO];
	if (!jsonString) {
		jsonString = [self flatten];
		jsonString = [jsonString compactString];
	}
	if ([jsonString isKindOfClass:[NSString class]]) {
		NSDictionary *attributes = @{ NSFontAttributeName:[UIFont boldSystemFontOfSize:13.0], NSForegroundColorAttributeName:color, NSParagraphStyleAttributeName:paragraphStyle };
		NSAttributedString *attributedStringFragment = [[NSMutableAttributedString alloc] initWithString:jsonString attributes:attributes];
		return attributedStringFragment;
	}
	return nil;
}
- (NSAttributedString *)attributedStringWithColor:(UIColor *)color backgroundColor:(UIColor*)backgroundColor{
	NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
	paragraphStyle.lineBreakMode = NSLineBreakByClipping;
	[paragraphStyle setAlignment:NSTextAlignmentLeft];
    
	NSString *jsonString = [self jsonStringWithPrettyPrint:NO];
	if (!jsonString) {
		jsonString = [self flatten];
		jsonString = [jsonString compactString];
	}
	if ([jsonString isKindOfClass:[NSString class]]) {
		NSDictionary *attributes = @{ NSFontAttributeName:[UIFont boldSystemFontOfSize:13.0],
                                      NSForegroundColorAttributeName:color,
                                      NSBackgroundColorAttributeName:backgroundColor,
                                      NSParagraphStyleAttributeName:paragraphStyle ,
                                      };
		NSAttributedString *attributedStringFragment = [[NSMutableAttributedString alloc] initWithString:jsonString attributes:attributes];
		return attributedStringFragment;
	}
	return nil;
}


@end
