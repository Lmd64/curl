//
//  NSObject+MethodExchange.h
//  Homestay
//
//  Created by Liam Dunne on 02/01/2014.
//  Copyright (c) 2014 Liam Dunne. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Extensions)
+ (void)exchangeMethod:(SEL)origSel withNewMethod:(SEL)newSel;
- (NSString *)stringFromObject;
- (NSString *)flatten;
- (NSString *)jsonString;
- (void)logUndefinedKey:(NSString*)key;
@end
