//
//  ViewController.m
// CURL
//
//  Created by Liam Dunne on 06/04/2014.
//  Copyright (c) 2014 Lmd64. All rights reserved.
//

#import <MobileCoreServices/MobileCoreServices.h>
#import "RequestViewController.h"
#import "UIFont+CustomFonts.h"
#import "Request+Helper.h"
#import "Format.h"
#import "JSONSyntaxHighlight.h"
#import "NSObject+MethodExchange.h"
#import "NSString+KeyValues.h"
#import "NSURLRequest+Extensions.h"

#define ONLY_SHOW_FIRST_ITEM NO
#define TRUNCATE_ITEM NO

@implementation UITextField (Padding)
+ (void)load {
    [[UITextField class] exchangeMethod:@selector(textRectForBounds:) withNewMethod:@selector(padding_textRectForBounds:)];
    [[UITextField class] exchangeMethod:@selector(editingRectForBounds:) withNewMethod:@selector(padding_editingRectForBounds:)];
}

- (CGRect)padding_textRectForBounds:(CGRect)bounds {
    return CGRectInset(bounds, 5.0f, 0.0f);
}

- (CGRect)padding_editingRectForBounds:(CGRect)bounds {
    return [self textRectForBounds:bounds];
}

@end

@interface RequestViewController () {
    UITextView *copiedTextView;
    UILabel *copiedLabel;
    UIView *copiedView;
    NSArray *titleColors;
    NSArray *subtitleColors;
    NSArray *headerTypes;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headContentWidthConstraint;
@property (nonatomic,strong) NSURLSession *session;

@property (nonatomic, strong) UIActivityIndicatorView *activityIndicatorView;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonatomic, strong) NSArray *methods;
@property (nonatomic, strong) NSArray *parsedResults;
@end

@implementation RequestViewController

- (NSString *)curlForRequest:(NSURLRequest*)request withSession:(NSURLSession*)session {
    __block NSString *log = [NSString stringWithFormat:@"curl -s -k"];
    NSString *authorization = [[[session configuration] HTTPAdditionalHeaders] valueForKey:@"Authorization"];
    if (authorization){
        log = [log stringByAppendingFormat:@" -H \"%@: %@\"", @"Authorization", authorization];
    } else {
        
    }
    if (![request.HTTPMethod isEqualToString:@"GET"]){
        log = [log stringByAppendingFormat:@" --request %@ ", request.HTTPMethod];
    }
    [[request allHTTPHeaderFields] enumerateKeysAndObjectsUsingBlock: ^(id key, id obj, BOOL *stop) {
        log = [log stringByAppendingFormat:@" -H \"%@: %@\"", key, obj];
    }];
    log = [log stringByAppendingFormat:@" \"%@\"", request.URL.absoluteString];
    NSString *data = [[NSString alloc] initWithData:request.HTTPBody encoding:NSUTF8StringEncoding];
    if (data && data.length) {
        data = [[data flatten] compactString];
        NSString *escapedData = data;//[data stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""];
        log = [log stringByAppendingFormat:@" --data '%@'", escapedData];
        //        id jsonObject = [NSJSONSerialization JSONObjectWithData:request.HTTPBody options:NSJSONReadingAllowFragments error:nil];
        //        log = [log stringByAppendingFormat:@"    HTTPBody: %@",[[jsonObject jsonString] flatten]];
    }
    CurlLog(@"%@",log);
    return log;
}


#pragma lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view, typically from a nib.
    
    
    NSURLSessionConfiguration *oauthConfigObject = [[NSURLSessionConfiguration defaultSessionConfiguration] copy];
    oauthConfigObject.requestCachePolicy = NSURLRequestReloadIgnoringCacheData;;
    oauthConfigObject.timeoutIntervalForResource = 10.0;
    self.session = [NSURLSession sessionWithConfiguration:oauthConfigObject delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    [self loadPanelController];
    
    CGFloat saturation = 0.7;
    CGFloat brightness = 0.5;
    CGFloat alpha = 1.0;
    NSMutableArray *mutableArray = [NSMutableArray array];
    int i = 0;
    while (i<3) {
        CGFloat hue = (float)i / (float)6.0;
        UIColor *_color = [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:alpha];
        [mutableArray addObject:_color];
        i++;
    }
    titleColors = [mutableArray copy];
    
    mutableArray = [NSMutableArray array];
    while (i<6) {
        CGFloat hue = (float)i / (float)6.0;
        UIColor *_color = [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:alpha];
        [mutableArray addObject:_color];
        i++;
    }
    subtitleColors = [mutableArray copy];
    
    self.title = NSLocalizedString(@"request", nil);
    self.parsedResults = @[];
    
    [self.resultTextView setFont:[UIFont systemFontOfSize:self.resultTextView.font.pointSize]];
    [self.resultTextView setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.8]];
    
    [self.bodyScrollView setBackgroundColor:kColorDefault];
    
    NSArray *buttons = @[self.CURLcopyButton, self.JSONcopyButton, self.shareButton];
    [buttons enumerateObjectsUsingBlock: ^(UIButton *button, NSUInteger idx, BOOL *stop) {
        [button.layer setCornerRadius:0.0];
        [button setBackgroundColor:[UIColor whiteColor]];
        [button setTitleColor:kColorDefault forState:UIControlStateNormal];
        [button setTitleColor:[kColorDefault colorWithAlphaComponent:0.5] forState:UIControlStateHighlighted];
        [button.titleLabel setFont:[UIFont boldSystemFontOfSize:button.titleLabel.font.pointSize]];
    }];
    [self.CURLcopyButton.titleLabel setNumberOfLines:2];
    [self.JSONcopyButton.titleLabel setNumberOfLines:2];
    [self.CURLcopyButton setTitle:NSLocalizedString(@"copy CURL", nil) forState:UIControlStateNormal];
    [self.JSONcopyButton setTitle:NSLocalizedString(@"copy JSON", nil) forState:UIControlStateNormal];
    [self.shareButton setTitle:NSLocalizedString(@"Share", nil) forState:UIControlStateNormal];
    
    [self.shareButton setHidden:YES];
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl setTintColor:kColorDefault];
    [refreshControl addTarget:self action:@selector(goButtonTapped:) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;
    [self.resultsTableView addSubview:refreshControl];
    
    [self.formatSwitch setTintColor:[UIColor colorWithWhite:1.0 alpha:0.25]];
    [self.formatSwitch setOnTintColor:[UIColor colorWithWhite:1.0 alpha:0.5]];
    [self.formatSwitch addTarget:self action:@selector(setDisplayParsedOutput) forControlEvents:UIControlEventValueChanged];
    [self.formatSwitchLabel setTextColor:[UIColor whiteColor]];
    [self.formatSwitchLabel setFont:[UIFont boldSystemFontOfSize:self.formatSwitchLabel.font.pointSize]];
    [self.formatSwitch setOn:[self.request.displayParsedOutput boolValue]];
    
    [self.panelViewController.title1Label setText:@"T1"];
    [self.panelViewController.title2Label setText:@"T2"];
    [self.panelViewController.title3Label setText:@"T3"];
    [self.panelViewController.subtitle1Label setText:@"S1"];
    [self.panelViewController.subtitle2Label setText:@"S2"];
    [self.panelViewController.subtitle3Label setText:@"S3"];
    
    [self.panelViewController.title1TextField setText:self.request.format.title1];
    [self.panelViewController.title2TextField setText:self.request.format.title2];
    [self.panelViewController.title3TextField setText:self.request.format.title3];
    [self.panelViewController.subtitle1TextField setText:self.request.format.subtitle1];
    [self.panelViewController.subtitle2TextField setText:self.request.format.subtitle2];
    [self.panelViewController.subtitle3TextField setText:self.request.format.subtitle3];
    
    [self.panelViewController.title1TextField setBackgroundColor:titleColors[0]];
    [self.panelViewController.title2TextField setBackgroundColor:titleColors[1]];
    [self.panelViewController.title3TextField setBackgroundColor:titleColors[2]];
    [self.panelViewController.subtitle1TextField setBackgroundColor:subtitleColors[0]];
    [self.panelViewController.subtitle2TextField setBackgroundColor:subtitleColors[1]];
    [self.panelViewController.subtitle3TextField setBackgroundColor:subtitleColors[2]];
    
    [self reloadData];
}

- (void)viewDidAppear:(BOOL)animated {
    [self animateOnFirstTimeUse];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    CGFloat panelWidth = CGRectGetWidth(self.view.frame);
    CGFloat contentWidth = panelWidth * self.panelViewController.panelCount;
    
    [self.headContentWidthConstraint setConstant:contentWidth];
    [self.headContentView layoutSubviews];
    
    self.bodyScrollView.contentSize = self.contentView.bounds.size;
    self.headScrollView.contentSize = self.headContentView.bounds.size;
    
    if (copiedView) {
        NSArray *subviews = [copiedView subviews];
        for (UIView *view in subviews) {
            [view setClipsToBounds:YES];
            [view.layer setCornerRadius:5.0];
            [view.layer setShadowColor:[UIColor blackColor].CGColor];
            [view.layer setShadowOffset:CGSizeMake(0, 2)];
            [view.layer setShadowOpacity:0.3];
            [view.layer setShadowRadius:1.0];
        }
    }
}

- (void)loadPanelController{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    RequestPanelViewController *controller = (RequestPanelViewController*)[storyboard instantiateViewControllerWithIdentifier:@"RequestPanelViewController"];
    self.panelViewController = controller;
    [self addChildViewController:self.panelViewController];
    [self.panelViewController setDelegate:self];
    [self.panelViewController setRequest:self.request];

    [self.panelViewController.view setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.headContentView addSubview:self.panelViewController.view];
    
    NSDictionary *views = @{@"view":self.panelViewController.view};
    NSDictionary *metrics = @{};
    [self.headContentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:metrics views:views]];
    [self.headContentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:metrics views:views]];
    [self.panelViewController.view layoutSubviews];
}

- (void)viewWillDisappear:(BOOL)animated {
    if ([self validateRequest]) {
        [self updateRequest];
        [self.request save];
    }
    if ([self.delegate respondsToSelector:@selector(reloadData)]) {
        [self.delegate performSelector:@selector(reloadData)];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma load child view controllers
- (void)stopJiggle:(CALayer *)layer {
    [layer removeAnimationForKey:@"jiggle"];
}

- (void)startJiggle:(CALayer *)layer {
    const float amplitude = 128.0f; // degrees
    float r = (rand() / (float)RAND_MAX) - 0.5f;
    float angleInDegrees = amplitude * (1.0f + r * 0.1f);
    float animationRotate = angleInDegrees / 180. * M_PI; // Convert to radians
    
    NSTimeInterval duration = 0.1;
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.translation.x"];
    animation.duration = duration;
    animation.additive = YES;
    animation.autoreverses = YES;
    animation.repeatCount = FLT_MAX;
    animation.fromValue = @(-animationRotate);
    animation.toValue = @(animationRotate);
    animation.timeOffset = (rand() / (float)RAND_MAX) * duration;
    [layer addAnimation:animation forKey:@"jiggle"];
}

- (void)animateOnFirstTimeUse {
    if ([[NSUserDefaults standardUserDefaults] boolForKey:kFirstTimeUseAnimationOnRequestViewController]) {
        return;
    }
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kFirstTimeUseAnimationOnRequestViewController];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        CGFloat offset = 16.0;
        CGFloat duration = 0.67;
        CGFloat damping = 0.5;
        CGFloat velocity = 0.15;
        
        CGFloat headDelay = 0.0;
        CGFloat bodyDelay = 2.4;
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(headDelay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self startJiggle:self.headScrollView.layer];
        });
        [UIView animateWithDuration:duration delay:headDelay usingSpringWithDamping:damping initialSpringVelocity:velocity options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionAllowUserInteraction animations: ^{
            [self.headScrollView setContentOffset:CGPointMake(offset, 0.0)];
        } completion: ^(BOOL finished) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self stopJiggle:self.headScrollView.layer];
            });
            [UIView animateWithDuration:duration delay:0.5 usingSpringWithDamping:damping initialSpringVelocity:velocity options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionAllowUserInteraction animations: ^{
                [self.headScrollView setContentOffset:CGPointMake(0.0, 0.0)];
            } completion: ^(BOOL finished) {
            }];
        }];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(bodyDelay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self startJiggle:self.bodyScrollView.layer];
        });
        [UIView animateWithDuration:duration delay:bodyDelay usingSpringWithDamping:damping initialSpringVelocity:velocity options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionAllowUserInteraction animations: ^{
            [self.bodyScrollView setContentOffset:CGPointMake(offset, 0.0)];
        } completion: ^(BOOL finished) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self stopJiggle:self.bodyScrollView.layer];
            });
            [UIView animateWithDuration:duration delay:0.5 usingSpringWithDamping:damping initialSpringVelocity:velocity options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionAllowUserInteraction animations: ^{
                [self.bodyScrollView setContentOffset:CGPointMake(0.0, 0.0)];
            } completion: ^(BOOL finished) {
            }];
        }];
    });
}

#pragma mark - ibactions
- (IBAction)goButtonTapped:(id)sender {
    [self performFetchRequest];
}

- (void)performFetchRequest{
    
    [self.view endEditing:YES];
    
    if (![self validateRequest])
        return;
    
    [self updateRequest];
    self.request.lastDateExecuted = [NSDate date];
    [self.request save];
    
    if (!self.activityIndicatorView) {
        self.activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [self.activityIndicatorView startAnimating];
        [self.resultTextView addSubview:self.activityIndicatorView];
        [self.activityIndicatorView setCenter:CGPointMake(CGRectGetMidX(self.resultTextView.bounds), CGRectGetMidY(self.resultTextView.bounds))];
        
        [self.activityIndicatorView setAlpha:0.0];
        [UIView animateWithDuration:0.2 animations: ^{
            [self.activityIndicatorView setAlpha:1.0];
        } completion: ^(BOOL finished) {
        }];
    }
    
    [self performRequestWithCompletion: ^(NSData *data, NSURLResponse *response, NSError *error) {
        NSUInteger statusCode = [(NSHTTPURLResponse *)response statusCode];
        
        [self.refreshControl endRefreshing];
        
        [UIView animateWithDuration:0.2 animations: ^{
            [self.activityIndicatorView setAlpha:0.0];
        } completion: ^(BOOL finished) {
            [self.activityIndicatorView removeFromSuperview];
            self.activityIndicatorView = nil;
        }];
        
        
        if (statusCode < 200 || statusCode >= 300) {
            self.request.successful = @NO;
            [self.request save];
            
            NSDictionary *userInfo = @{ @"statusCode":@(statusCode), @"localizedStringForStatusCode":[NSHTTPURLResponse localizedStringForStatusCode:statusCode] };
            error = [NSError errorWithDomain:self.request.urlForRequest.absoluteString code:statusCode userInfo:userInfo];
            [self.resultTextView setText:error.localizedDescription];
            return;
        }
        else {
            self.request.successful = @YES;
            [self.request save];
            
            if ([data length] != 0) {
                NSError *jsonError;
                id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonError];
                JSONSyntaxHighlight *jsh = [[JSONSyntaxHighlight alloc] initWithJSON:jsonObject];
                
                NSAttributedString *attributedString;
                attributedString = [jsh highlightJSONWithPrettyPrint:YES];
                
                [self.resultTextView setAttributedText:attributedString];
                [self parseResults:data];
            }
            else {
                [self.resultTextView setText:@"<NULL>"];
            }
        }
    }];
}

- (void)performRequestWithCompletion:(CURLURLSessionResponseHandler)completion {

    NSMutableURLRequest *mutableURLRequest = [[self.request URLRequest] mutableCopy];
    if (self.request.headerType && self.request.header){
        NSString *oauthString = [NSString stringWithFormat:@"%@ %@",self.request.headerType,self.request.header];
        [mutableURLRequest addValue:oauthString forHTTPHeaderField:@"Authorization"];
    }
    
    if (![self.request.method isEqualToString:@"GET"]) {
        NSString *httpBody = self.request.body;
        NSData *httpBodyData = [httpBody dataUsingEncoding:NSASCIIStringEncoding];
        if (httpBodyData.length > 0) {
            id JSONObject = [NSJSONSerialization JSONObjectWithData:httpBodyData options:0 error:nil];
            NSError *error;
            NSData *data = [NSJSONSerialization dataWithJSONObject:JSONObject options:NSJSONWritingPrettyPrinted error:&error];
            [mutableURLRequest setHTTPBody:data];
        }
        [mutableURLRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    }

    NSURLRequest *request = [mutableURLRequest copy];
    
    CLog(@"");
    [self curlForRequest:request  withSession:self.session];

    NSURLSessionTask *task = [self.session dataTaskWithRequest:request completionHandler: ^(NSData *data, NSURLResponse *response, NSError *error) {
        [request logData:data response:response error:error];
        CLog(@"");
        if (completion) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(data, response, error);
            });
        }
    }];
    [task resume];
    NSParameterAssert(task);
}

- (void)parseResults:(NSData *)data {
    NSError *error;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
    
    NSArray *response = (NSArray *)jsonObject;
    if ([response isKindOfClass:[NSDictionary class]]){
        NSDictionary *dictionary = (NSDictionary*)response;
        //grab the first value from the dictionary if there's only only key/value pair
        //(ie, discard the data key, just use the value)
        if ([[dictionary allKeys] count]==1){
            response = [[dictionary allValues] firstObject];
        } else {
            response = @[response];
        }
    }
    
    self.parsedResults = [response copy];
    
    //attempt to sort by date descending
    NSDictionary *firstObject = [self.parsedResults firstObject];
    if ([firstObject isKindOfClass:[NSDictionary class]]) {
        __block NSString *sortKey = nil;
        [[firstObject allKeys] enumerateObjectsUsingBlock: ^(NSString *key, NSUInteger idx, BOOL *stop) {
            id value = [firstObject valueForKey:key];
            if ([value isKindOfClass:[NSString class]] || [value isKindOfClass:[NSNumber class]]) {
                if ([[key lowercaseString] rangeOfString:@"date"].location != NSNotFound) {
                    //match, sort by this
                    sortKey = key;
                    *stop = YES;
                }
            }
        }];
        if (sortKey) {
            self.parsedResults = [self.parsedResults sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:sortKey ascending:NO]]];
        }
    }
    
    self.title = [NSString localizedStringWithFormat:@"request (%@)", @([self.parsedResults count])];
    
    [self.resultsTableView reloadData];
}

#pragma mark - load core data
- (void)reloadData {
    NSInteger idx = [self.methods indexOfObject:self.request.method];
    if (idx > -1) {
        self.panelViewController.methodSegmentedControl.selectedSegmentIndex = idx;
    }
    self.panelViewController.actionTextField.text = self.request.action;
    self.panelViewController.bodyTextField.text = self.request.body;
    
    //only automatically execute if it's a GET
    if ([self.request.method isEqualToString:@"GET"]) {
        [self goButtonTapped:nil];
    }
    [self setDisplayParsedOutput];
}

- (void)setDisplayParsedOutput {
    CGFloat alpha = 0.0;
    if (self.formatSwitch.isOn) {
        alpha = 1.0;
    }
    [UIView animateWithDuration:0.25 animations: ^{
        [self.resultTextView setAlpha:1.0-alpha];
        [self.resultsTableView setAlpha:alpha];
    }];
}

#pragma mark - update core data
- (BOOL)validateRequest {
    if (self.panelViewController.methodSegmentedControl.selectedSegmentIndex == -1) return NO;
    if (self.panelViewController.actionTextField.text.length == 0) return NO;
    return YES;
}

- (void)updateRequest {
    self.request.method = self.panelViewController.method;
    self.request.endpoint = [[NSUserDefaults standardUserDefaults] valueForKey:kEndpoint];
    self.request.action = [self.panelViewController.actionTextField.text compactString];
    self.request.body = [self.panelViewController.bodyTextField.text compactString];
    self.request.displayParsedOutput = @(self.formatSwitch.on);
    
    self.request.format.title1 = [self.panelViewController.title1TextField.text compactString];
    self.request.format.title2 = [self.panelViewController.title2TextField.text compactString];
    self.request.format.title3 = [self.panelViewController.title3TextField.text compactString];
    self.request.format.subtitle1 = [self.panelViewController.subtitle1TextField.text compactString];
    self.request.format.subtitle2 = [self.panelViewController.subtitle2TextField.text compactString];
    self.request.format.subtitle3 = [self.panelViewController.subtitle3TextField.text compactString];
    
    [self.request save];
    
    //update defaults
    [[NSUserDefaults standardUserDefaults] setValue:self.request.endpoint forKeyPath:@"endpoint"];
    [[NSUserDefaults standardUserDefaults] setValue:self.request.action forKeyPath:@"action"];
    [[NSUserDefaults standardUserDefaults] setValue:self.request.method forKeyPath:@"method"];
    
    [[NSUserDefaults standardUserDefaults] setValue:self.request.format.title1 forKeyPath:@"title1"];
    [[NSUserDefaults standardUserDefaults] setValue:self.request.format.title2 forKeyPath:@"title2"];
    [[NSUserDefaults standardUserDefaults] setValue:self.request.format.title3 forKeyPath:@"title3"];
    [[NSUserDefaults standardUserDefaults] setValue:self.request.format.subtitle1 forKeyPath:@"subtitle1"];
    [[NSUserDefaults standardUserDefaults] setValue:self.request.format.subtitle2 forKeyPath:@"subtitle2"];
    [[NSUserDefaults standardUserDefaults] setValue:self.request.format.subtitle3 forKeyPath:@"subtitle3"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self.resultsTableView reloadData];
}

#pragma mark - DelegateMethods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([self.parsedResults count])
        return [self.parsedResults count];
    else
        return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 64;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.parsedResults count] == 0) {
        static NSString *kTableviewcellEmpty = @"kTableviewcellEmpty";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTableviewcellEmpty];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:kTableviewcellEmpty];
        }
        [self configureEmptyDataCell:cell indexPath:indexPath];
        return cell;
    }
    else {
        static NSString *kTableviewcell = @"kTableviewcell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTableviewcell];
        if (!cell) {
            if ([self.request hasSubtitle]) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:kTableviewcell];
            }
            else {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kTableviewcell];
            }
        }
        
        [self tableView:tableView configureCell:cell indexPath:indexPath];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self.view endEditing:YES];
    
    NSInteger row = [indexPath row];
    NSDictionary *dictionary = self.parsedResults[row];
    NSString *key = self.request.format.title1;
    if (!key || [key isEqualToString:@""]) key = self.request.format.title2;
    if (!key || [key isEqualToString:@""]) key = self.request.format.title3;
    if (!key || [key isEqualToString:@""]) key = self.request.format.subtitle1;
    if (!key || [key isEqualToString:@""]) key = self.request.format.subtitle2;
    if (!key || [key isEqualToString:@""]) key = self.request.format.subtitle3;
    if (!key) return;
    id value = [dictionary valueForKeyPath:key];
    
    value = [value stringFromObject];
    if (!value) return;
    
    value = [value compactString];
    if (!value) return;
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = value;
    
    key = [key stringByAppendingString:@": "];
    
    NSMutableAttributedString *mutableAttributedString = [[NSMutableAttributedString alloc] init];
    [mutableAttributedString appendAttributedString:[[NSAttributedString alloc] initWithString:key attributes:@{ NSForegroundColorAttributeName:[kColorDefault colorWithAlphaComponent:0.75], NSFontAttributeName:[UIFont systemFontOfSize:18.0] }]];
    [mutableAttributedString appendAttributedString:[[NSAttributedString alloc] initWithString:value attributes:@{ NSForegroundColorAttributeName:kColorDefault, NSFontAttributeName:[UIFont systemFontOfSize:18.0] }]];
    
    [self displayCopiedText:mutableAttributedString duration:0.5];
}

- (void)configureEmptyDataCell:(UITableViewCell *)cell indexPath:(NSIndexPath *)indexPath {
    [cell.textLabel setText:NSLocalizedString(@"No results", nil)];
    [cell.textLabel setTextColor:[UIColor whiteColor]];
    [cell.contentView setBackgroundColor:[kColorDefault colorWithAlphaComponent:0.25]];
}

- (void)tableView:(UITableView *)tableView configureCell:(UITableViewCell *)cell indexPath:(NSIndexPath *)indexPath {
    NSInteger row = [indexPath row];
    NSDictionary *dictionary = [self.parsedResults[row] copy];
    
    [self.request updateFormat];
    
    NSString *title1key = [self.request.format.title1 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *title2key = [self.request.format.title2 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *title3key = [self.request.format.title3 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *subtitle1key = [self.request.format.subtitle1 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *subtitle2key = [self.request.format.subtitle2 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *subtitle3key = [self.request.format.subtitle3 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSString *title1index = [NSString stringWithFormat:@"%@", @(row)];
    NSString *title1value = nil;
    NSString *title2value = nil;
    NSString *title3value = nil;
    NSString *subtitle1value = nil;
    NSString *subtitle2value = nil;
    NSString *subtitle3value = nil;
    
    title1value = [dictionary safeValueForKeyPath:title1key];
    title2value = [dictionary safeValueForKeyPath:title2key];
    title3value = [dictionary safeValueForKeyPath:title3key];
    subtitle1value = [dictionary safeValueForKeyPath:subtitle1key];
    subtitle2value = [dictionary safeValueForKeyPath:subtitle2key];
    subtitle3value = [dictionary safeValueForKeyPath:subtitle3key];
    
    //	title1value = [dictionary valueForKeyPath:title1key];
    //	title2value = [dictionary valueForKeyPath:title2key];
    //	title3value = [dictionary valueForKeyPath:title3key];
    //	subtitle1value = [dictionary valueForKeyPath:subtitle1key];
    //	subtitle2value = [dictionary valueForKeyPath:subtitle2key];
    //	subtitle3value = [dictionary valueForKeyPath:subtitle3key];
    
    if ([title1value isKindOfClass:[NSNumber class]]) title1value = [NSString stringWithFormat:@"%@", title1value];
    if ([title2value isKindOfClass:[NSNumber class]]) title2value = [NSString stringWithFormat:@"%@", title2value];
    if ([title3value isKindOfClass:[NSNumber class]]) title3value = [NSString stringWithFormat:@"%@", title3value];
    if ([subtitle1value isKindOfClass:[NSNumber class]]) subtitle1value = [NSString stringWithFormat:@"%@", subtitle1value];
    if ([subtitle2value isKindOfClass:[NSNumber class]]) subtitle2value = [NSString stringWithFormat:@"%@", subtitle2value];
    if ([subtitle3value isKindOfClass:[NSNumber class]]) subtitle3value = [NSString stringWithFormat:@"%@", subtitle3value];
    
    NSMutableArray *mutableTitles = [NSMutableArray array];
    [mutableTitles addObject:title1index];
    if (title1value) [mutableTitles addObject:title1value];
    if (title2value) [mutableTitles addObject:title2value];
    if (title3value) [mutableTitles addObject:title3value];
    
    NSMutableArray *mutableSubtitles = [NSMutableArray array];
    if (subtitle1value) [mutableSubtitles addObject:subtitle1value];
    if (subtitle2value) [mutableSubtitles addObject:subtitle2value];
    if (subtitle3value) [mutableSubtitles addObject:subtitle3value];
    
    __block NSMutableAttributedString *attributedString;
    
    NSUInteger maxStringLength = 64;
    
    attributedString = [[NSMutableAttributedString alloc] init];
    [mutableTitles enumerateObjectsUsingBlock: ^(NSString *string, NSUInteger idx, BOOL *stop) {
        if ([string isKindOfClass:[NSArray class]]) {
            if (ONLY_SHOW_FIRST_ITEM) {
                NSArray *array = (NSArray *)string;
                NSUInteger count = [array count];
                string = [[[array firstObject] flatten] compactString];
                if (count > 1) {
                    string = [NSString stringWithFormat:@"%@ [and %@ more]", string, @(count - 1)];
                }
            }
            else {
                string = [(NSArray *)string componentsJoinedByString : @", "];
            }
        }
        else if ([string isKindOfClass:[NSDictionary class]]) {
            string = [[string flatten] compactString];
        }
        if ([string isKindOfClass:[NSString class]]) {
            if (TRUNCATE_ITEM) {
                if (string.length > maxStringLength) {
                    string = [string substringToIndex:maxStringLength];
                    string = [string stringByAppendingString:@"..."];
                }
            }
            CGFloat alpha = 1.0 - 0.1 * idx;
            UIColor *color = [kColorDefault colorWithAlphaComponent:alpha];
            if (idx == 0) {
                NSAttributedString *attributedStringFragment = [string attributedStringWithColor:kColorDefault];
                [attributedString appendAttributedString:attributedStringFragment];
            }
            else {
                UIColor *color = [titleColors[idx - 1] colorWithAlphaComponent:1.0];
                NSAttributedString *attributedStringFragment = [string attributedStringWithColor:[UIColor whiteColor] backgroundColor:color];
                [attributedString appendAttributedString:attributedStringFragment];
            }
            
            string = nil;
            if (idx == 0) {
                string = @": ";
            }
            else if (idx != [mutableTitles count] - 1) {
                string = @", ";
            }
            if (string) {
                //UIColor *backgroundColor = [subtitleColors[idx] colorWithAlphaComponent:1.0];
                NSAttributedString *attributedStringFragment = [string attributedStringWithColor:color backgroundColor:[UIColor clearColor]];
                [attributedString appendAttributedString:attributedStringFragment];
            }
        }
    }];
    [cell.textLabel setAttributedText:attributedString];
    
    if ([self.request hasSubtitle]) {
        attributedString = [[NSMutableAttributedString alloc] init];
        [mutableSubtitles enumerateObjectsUsingBlock: ^(NSString *string, NSUInteger idx, BOOL *stop) {
            if ([string isKindOfClass:[NSArray class]]) {
                if (ONLY_SHOW_FIRST_ITEM) {
                    NSArray *array = (NSArray *)string;
                    NSUInteger count = [array count];
                    string = [[[array firstObject] flatten] compactString];
                    if (count > 1) {
                        string = [NSString stringWithFormat:@"%@ [and %@ more]", string, @(count - 1)];
                    }
                }
                else {
                    string = [(NSArray *)string componentsJoinedByString : @", "];
                }
                string = [NSString stringWithFormat:@"[%@]", string];
            }
            else if ([string isKindOfClass:[NSDictionary class]]) {
                string = [[string flatten] compactString];
            }
            //CGFloat alpha = 1.0 - 0.1 * idx;
            UIColor *color = [subtitleColors[idx] colorWithAlphaComponent:1.0];
            if ([string isKindOfClass:[NSString class]]) {
                if (TRUNCATE_ITEM) {
                    if (string.length > maxStringLength) {
                        string = [string substringToIndex:maxStringLength];
                        string = [string stringByAppendingString:@"..."];
                    }
                }
                NSAttributedString *attributedStringFragment = [string attributedStringWithColor:[UIColor whiteColor] backgroundColor:color];
                [attributedString appendAttributedString:attributedStringFragment];
            }
            
            if (idx != [mutableTitles count] - 1) {
                string = @", ";
                NSAttributedString *attributedStringFragment = [string attributedStringWithColor:color backgroundColor:[UIColor clearColor]];
                [attributedString appendAttributedString:attributedStringFragment];
            }
        }];
        [cell.detailTextLabel setAttributedText:attributedString];
    }
}

#pragma mark - share actions
- (IBAction)CURLcopyButtonTapped:(id)sender {
    NSString *copiedText = [self.request curlRequest];
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = copiedText;
    
    [self displayCopiedText:[[NSAttributedString alloc] initWithString:copiedText attributes:@{ NSForegroundColorAttributeName:kColorDefault, NSFontAttributeName:[UIFont systemFontOfSize:14.0] }] duration:2.5];
}

- (IBAction)JSONcopyButtonTapped:(id)sender {
    NSAttributedString *copiedText = self.resultTextView.attributedText;
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    
    NSData *rtf = [copiedText dataFromRange:NSMakeRange(0, copiedText.length) documentAttributes:@{ NSDocumentTypeDocumentAttribute: NSRTFTextDocumentType } error:nil];
    pasteboard.items = @[@{ (id)kUTTypeRTF:[[NSString alloc] initWithData:rtf encoding:NSUTF8StringEncoding],
                            (id)kUTTypeUTF8PlainText:copiedText.string }];
    
    [self displayCopiedText:copiedText duration:2.5];
}

- (IBAction)shareButtonTapped:(id)sender {
}

- (void)displayCopiedText:(NSAttributedString *)copiedText duration:(NSTimeInterval)duration {
    copiedView = [[UIView alloc] initWithFrame:self.bodyScrollView.frame];
    [copiedView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.667]];
    [self.view addSubview:copiedView];
    
    copiedTextView = [[UITextView alloc] init];
    [copiedTextView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [copiedTextView setAttributedText:copiedText];
    [copiedTextView setUserInteractionEnabled:NO];
    [copiedView addSubview:copiedTextView];
    
    //[copiedView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[copiedTextView(maxHeight)]" options:0 metrics:@{ @"maxHeight":@(maxHeight) } views:@{ @"copiedTextView":copiedTextView }]];
    [copiedView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[copiedTextView]-|" options:0 metrics:nil views:@{ @"copiedTextView":copiedTextView }]];
    [copiedView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[copiedTextView]-|" options:0 metrics:nil views:@{ @"copiedTextView":copiedTextView }]];
    [copiedTextView setContentHuggingPriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];
    [copiedTextView setContentHuggingPriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisVertical];
    [copiedTextView setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];
    [copiedTextView setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisVertical];
    
    copiedLabel = [[UILabel alloc] init];
    [copiedLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [copiedLabel setNumberOfLines:0];
    [copiedLabel setBackgroundColor:kColorDefault];
    [copiedLabel setTextColor:[UIColor whiteColor]];
    [copiedLabel setText:NSLocalizedString(@"copied", nil)];
    [copiedLabel setTextAlignment:NSTextAlignmentCenter];
    [copiedLabel setFont:[UIFont boldSystemFontOfSize:40.0]];
    [copiedLabel setAlpha:0.75];
    [copiedLabel setUserInteractionEnabled:NO];
    [copiedView addSubview:copiedLabel];
    
    NSDictionary *metrics = @{ @"pad":@25 };
    [copiedView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[copiedLabel]-(pad)-|" options:0 metrics:metrics views:@{ @"copiedLabel":copiedLabel }]];
    [copiedView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(pad)-[copiedLabel]-(pad)-|" options:0 metrics:metrics views:@{ @"copiedLabel":copiedLabel }]];
    [copiedLabel setContentHuggingPriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal | UILayoutConstraintAxisVertical];
    [copiedLabel setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal | UILayoutConstraintAxisVertical];
    
    [copiedView setAlpha:0.0];
    [UIView animateWithDuration:0.3 animations: ^{
        [copiedView setAlpha:1.0];
    } completion: ^(BOOL finished) {
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapView:)];
        [copiedView addGestureRecognizer:tap];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(duration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self removeView:copiedView];
        });
    }];
}

- (void)tapView:(UITapGestureRecognizer *)tap {
    if (tap.state == UIGestureRecognizerStateEnded) {
        [self removeView:tap.view];
    }
}

- (void)removeView:(UIView *)view {
    [UIView animateWithDuration:0.3 animations: ^{
        [view setAlpha:0.0];
    } completion: ^(BOOL finished) {
        [view removeFromSuperview];
    }];
}

#pragma mark - scrollview methods
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if ([scrollView isEqual:self.bodyScrollView]) {
        scrollView.contentOffset = CGPointMake(MAX(scrollView.contentOffset.x, 0), 0);
    }
    else if ([scrollView isEqual:self.headScrollView]) {
        scrollView.contentOffset = CGPointMake(MAX(scrollView.contentOffset.x, 0), 0);
    }
    if ([scrollView isEqual:self.resultsTableView] || [scrollView isEqual:self.bodyScrollView]) {
        [self.view endEditing:YES];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if ([scrollView isEqual:self.headScrollView]) {
        if (scrollView.contentOffset.x < 100) {
            [scrollView setContentOffset:CGPointZero animated:YES];
        }
    }
}

#pragma mark - actionsheet delegate methods
- (IBAction)didTapHeaderTypeButton:(id)sender{
    [self.view endEditing:YES];
    headerTypes = @[@"Bearer",
                    @"Authorization",
                    ];
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    [actionSheet setDelegate:self];
    
    [headerTypes enumerateObjectsUsingBlock:^(NSString *headerType, NSUInteger idx, BOOL *stop){
        [actionSheet addButtonWithTitle:headerType];
    }];
    [actionSheet addButtonWithTitle:NSLocalizedString(@"None", nil)];
    [actionSheet setCancelButtonIndex:headerTypes.count];
    
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex!=headerTypes.count){
        [self.panelViewController setHeaderTypeButtonTitle:headerTypes[buttonIndex]];
    } else {
        [self.panelViewController setHeaderTypeButtonTitle:nil];
    }
}


- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential *credential))completionHandler {
    NSURLCredential *credential = [NSURLCredential
                                   credentialWithUser:nil
                                   password:nil
                                   persistence:NSURLCredentialPersistencePermanent];
    [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    completionHandler(NSURLSessionAuthChallengeUseCredential, credential);
}


@end
