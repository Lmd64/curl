//
//  Format.h
//  curl
//
//  Created by Liam Dunne on 08/04/2014.
//  Copyright (c) 2014 Lmd64. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Request;

@interface Format : NSManagedObject

@property (nonatomic, retain) NSString * title1;
@property (nonatomic, retain) NSString * title2;
@property (nonatomic, retain) NSString * title3;
@property (nonatomic, retain) NSString * subtitle1;
@property (nonatomic, retain) NSString * subtitle2;
@property (nonatomic, retain) NSString * subtitle3;
@property (nonatomic, retain) Request *request;

@end
