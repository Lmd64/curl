//
//  RequestPanelViewController.m
//  curly
//
//  Created by Liam Dunne on 08/02/2015.
//  Copyright (c) 2015 lmd64. All rights reserved.
//

#import "RequestPanelViewController.h"
#import "Request+Helper.h"
#import "Format.h"
#import "NSString+KeyValues.h"

@interface RequestPanelViewController () {
    NSArray *labels;
    NSArray *textFields;
}
@property (nonatomic,strong) NSArray *methods;
@end

@implementation RequestPanelViewController

#pragma mark - lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.panelCount = 2;
    [self setup];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setup{
    labels = @[
               self.title1Label,
               self.title2Label,
               self.title3Label,
               self.subtitle1Label,
               self.subtitle2Label,
               self.subtitle3Label,
               ];
    textFields = @[
                   self.title1TextField,
                   self.title2TextField,
                   self.title3TextField,
                   self.subtitle1TextField,
                   self.subtitle2TextField,
                   self.subtitle3TextField,
                   ];

    self.methods = @[@"GET",
                     @"PUT",
                     @"POST",
                     @"DELETE"
                     ];

    [self.methodSegmentedControl removeAllSegments];
    [self.methods enumerateObjectsUsingBlock: ^(NSString *title, NSUInteger idx, BOOL *stop) {
        [self.methodSegmentedControl insertSegmentWithTitle:title atIndex:idx animated:NO];
    }];
    [self.methodSegmentedControl setTintColor:kColorDefault];
    [self.methodSegmentedControl setMomentary:NO];
    
    [self.actionTextField setDelegate:self];
    [self.bodyTextField setDelegate:self];
    
    [self.actionTextField setFont:[UIFont systemFontOfSize:self.actionTextField.font.pointSize]];
    [self.bodyTextField setFont:[UIFont boldSystemFontOfSize:self.bodyTextField.font.pointSize]];
    
    [self.actionTextField setTextColor:[UIColor whiteColor]];
    [self.bodyTextField setTextColor:[UIColor whiteColor]];
    [self.actionTextField setBackgroundColor:kColorDefault];
    [self.bodyTextField setBackgroundColor:kColorDefault];
    
    [self.actionTextField.layer setCornerRadius:4.0];
    [self.bodyTextField.layer setCornerRadius:4.0];
    
    [self.headerTypeButton.titleLabel setFont:[UIFont systemFontOfSize:self.headerTypeButton.titleLabel.font.pointSize]];
    [self.headerTypeButton setTitleColor:kColorDefault forState:UIControlStateNormal];

    [self.headerTextField setFont:[UIFont systemFontOfSize:self.headerTextField.font.pointSize]];
    [self.headerTextField setTextColor:[UIColor whiteColor]];
    [self.headerTextField setBackgroundColor:kColorDefault];
    [self.headerTextField.layer setCornerRadius:4.0];
    [self.headerTextField setDelegate:self];
    
    [labels enumerateObjectsUsingBlock: ^(UILabel *view, NSUInteger idx, BOOL *stop) {
        [view setFont:[UIFont boldSystemFontOfSize:view.font.pointSize]];
        [view setTextColor:kColorDefault];
    }];
    [textFields enumerateObjectsUsingBlock: ^(UITextField *view, NSUInteger idx, BOOL *stop) {
        [view setFont:[UIFont systemFontOfSize:view.font.pointSize]];
        CGFloat alpha = 1.0 - (int)(idx / 2) * 0.25;
        [view setBackgroundColor:[kColorDefault colorWithAlphaComponent:alpha]];
        [view.layer setCornerRadius:4.0];
        [view setTextColor:[UIColor whiteColor]];
        [view setDelegate:self];
        [view setAutocorrectionType:UITextAutocorrectionTypeNo];
        [view setClearButtonMode:UITextFieldViewModeWhileEditing];
        [view setReturnKeyType:UIReturnKeyNext];
    }];

    //[self.methodSegmentedControl addTarget:self action:@selector(didChangeValueForMethodSegmentedControl:) forControlEvents:UIControlEventValueChanged];
}


- (void)reloadData{
    NSUInteger selectedSegmentIndex = [self.methods indexOfObject:self.request.method];
    [self.methodSegmentedControl setSelectedSegmentIndex:selectedSegmentIndex];

    [self.actionTextField setText:self.request.action];
    [self.bodyTextField setText:self.request.body];

    //set this first
    [self.headerTextField setText:self.request.header];
    [self setHeaderTypeButtonTitle:self.request.headerType];
    
    [self.title1TextField setText:self.request.format.title1];
    [self.title2TextField setText:self.request.format.title2];
    [self.title3TextField setText:self.request.format.title3];
    [self.subtitle1TextField setText:self.request.format.subtitle1];
    [self.subtitle2TextField setText:self.request.format.subtitle2];
    [self.subtitle3TextField setText:self.request.format.subtitle3];
}

#pragma mark - ibactions
- (IBAction)didTapHeaderTypeButton:(id)sender{
    if ([self.delegate respondsToSelector:@selector(didTapHeaderTypeButton:)]){
        [self.delegate performSelector:@selector(didTapHeaderTypeButton:) withObject:self.headerTypeButton];
    }
}

//- (void)didChangeValueForMethodSegmentedControl:(UISegmentedControl*)segmentedControl{
//    NSString *method = self.methods[segmentedControl.selectedSegmentIndex];
//    [self.request setMethod:method];
//    [self.request save];
//}

#pragma mark - textfield delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    BOOL shouldSelectAllText = ([textField isEqual:self.actionTextField] && [self.actionTextField.text isEqual:kDefaultAction]);
    if (shouldSelectAllText) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [textField setSelectedTextRange:[textField textRangeFromPosition:textField.beginningOfDocument toPosition:textField.endOfDocument]];
        });
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([textField isEqual:self.actionTextField]) {
        [self.bodyTextField becomeFirstResponder];
    }
    else if ([textField isEqual:self.bodyTextField]) {
        if ([self.delegate respondsToSelector:@selector(performFetchRequest)]){
            [self.delegate performSelector:@selector(performFetchRequest)];
        }
    }
    if ([textField isEqual:self.title1TextField]) {
        [self.title2TextField becomeFirstResponder];
    }
    else if ([textField isEqual:self.title2TextField]) {
        [self.title3TextField becomeFirstResponder];
    }
    else if ([textField isEqual:self.title3TextField]) {
        [self.subtitle1TextField becomeFirstResponder];
    }
    else if ([textField isEqual:self.subtitle1TextField]) {
        [self.subtitle2TextField becomeFirstResponder];
    }
    else if ([textField isEqual:self.subtitle2TextField]) {
        [self.subtitle3TextField becomeFirstResponder];
    }
    else if ([textField isEqual:self.subtitle3TextField]) {
        [self.title1TextField becomeFirstResponder];
    }
    [self updateRequest];
    return YES;
}

- (void)updateRequest{
    if ([self.delegate respondsToSelector:@selector(updateRequest)]){
        [self.delegate performSelector:@selector(updateRequest)];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    [self updateRequest];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [self updateRequest];
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    textField.text = [textField.text compactString];
    [self updateRequest];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    textField.text = [textField.text compactString];
    if ([textField isEqual:self.headerTextField]){
        self.request.header = textField.text;
        [self.request save];
    }
    [self updateRequest];
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    [self updateRequest];
    return YES;
}

- (void)setHeaderTypeButtonTitle:(NSString*)title{
    self.request.headerType = title;
    if (title){
        self.request.header = self.headerTextField.text;
    }
    [UIView animateWithDuration:0.2 animations:^{
        [self.headerTypeButton setAlpha:0.5];
    } completion:^(BOOL finished){

        if (title){
            [self.headerTypeButton setTitle:title forState:UIControlStateNormal];
            [self.headerTypeButton.titleLabel setFont:[UIFont boldSystemFontOfSize:self.headerTextField.font.pointSize]];
        } else {
            [self.headerTypeButton setTitle:NSLocalizedString(@"OAuth", nil) forState:UIControlStateNormal];
            [self.headerTypeButton.titleLabel setFont:[UIFont systemFontOfSize:self.headerTextField.font.pointSize]];
        }

        [UIView animateWithDuration:0.2 animations:^{
            [self.view.superview layoutIfNeeded];
            [self.headerTypeButton setAlpha:1.0];
        } completion:^(BOOL finished){
        }];

    }];

}

- (NSString*)method{
    return self.methods[self.methodSegmentedControl.selectedSegmentIndex];
}

@end
