//
//  RequestsTableViewController.h
// CURL
//
//  Created by Liam Dunne on 06/04/2014.
//  Copyright (c) 2014 Lmd64. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RequestViewController.h"

@interface RequestsTableViewController : UITableViewController <RequestEditorDelegate, UITextViewDelegate, UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UITextView *endpointTextView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *addBarButtonItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *helpBarButtonItem;

- (IBAction)addButtonTapped:(id)sender;
- (IBAction)helpButtonTapped:(id)sender;

@end
