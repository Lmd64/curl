//
//  RequestsTableViewController.m
// CURL
//
//  Created by Liam Dunne on 06/04/2014.
//  Copyright (c) 2014 Lmd64. All rights reserved.
//

//#import <os/activity.h>
#import "RequestsTableViewController.h"
#import "Request+Helper.h"
#import "NSObject+MethodExchange.h"

#define kRequesttableviewcell    @"requesttableviewcell"

@interface RequestsTableViewController () {
	NSFetchedResultsController *resultsController;
}

@end

@implementation RequestsTableViewController

- (id)initWithStyle:(UITableViewStyle)style {
	self = [super initWithStyle:style];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];

//    os_activity_set_breadcrumb("viewdidload completed");
//    os_trace("trace");
    
	NSString *endpoint = [[NSUserDefaults standardUserDefaults] valueForKey:kEndpoint];
	if (!endpoint) {
		endpoint = @"http://api.server.com/v1/";
		[[NSUserDefaults standardUserDefaults] setValue:endpoint forKey:kEndpoint];
	}

	[Request load];

	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadDataWithNotification:) name:kNotificationReloadData object:nil];

	self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
	self.navigationController.navigationBar.titleTextAttributes = @{ NSForegroundColorAttributeName: [UIColor whiteColor] };
	[self.navigationController.navigationBar setBarTintColor:kColorDefault];
	[self.navigationController.navigationBar setTranslucent:YES];

	[self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:kRequesttableviewcell];

	[self.endpointTextView setFont:[UIFont boldSystemFontOfSize:self.endpointTextView.font.pointSize]];
	[self.endpointTextView setTextColor:[UIColor whiteColor]];
	[self.endpointTextView setBackgroundColor:kColorDefault];



	[self reloadData];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (void)reloadData {
	NSString *endpoint = [[NSUserDefaults standardUserDefaults] valueForKey:kEndpoint];
	if (!endpoint) {
		endpoint = @"http://api.server.com/v1/";
	}
	self.endpointTextView.text = endpoint;


	[Request load];
	resultsController = [Request resultsController];

	[self.tableView reloadData];
}

// //////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Table view data source
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	// Return the number of sections.
	return [[resultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	// Return the number of rows in the section.
	id <NSFetchedResultsSectionInfo> sectionInfo = [resultsController sections][section];
	return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kRequesttableviewcell forIndexPath:indexPath];

	// Configure the cell...
	Request *request = [resultsController objectAtIndexPath:indexPath];

	[cell.textLabel setAttributedText:[request attributedString]];

	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	Request *request = [resultsController objectAtIndexPath:indexPath];
	[self performSegueWithIdentifier:@"RequestViewController" sender:request];
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
	// Return NO if you do not want the specified item to be editable.
	return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
	if (editingStyle == UITableViewCellEditingStyleDelete) {
		// Delete the row from the data source

		//remove from core data
		Request *request = [resultsController objectAtIndexPath:indexPath];
		[request delete];
		NSError *error;
		[resultsController performFetch:&error];

		//remove from tableview
		[tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
	}
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	if ([[segue identifier] isEqualToString:@"RequestViewController"]) {
		RequestViewController *requestViewController = (RequestViewController *)[segue destinationViewController];
		[requestViewController setDelegate:self];
		Request *request = (Request *)sender;
		requestViewController.request = request;

		UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
		self.navigationItem.backBarButtonItem = backButtonItem;
	}
}

- (IBAction)addButtonTapped:(id)sender {
	Request *request = [Request new];
	[self performSegueWithIdentifier:@"RequestViewController" sender:request];
	[self reloadData];
}

- (IBAction)helpButtonTapped:(id)sender {
	[self performSegueWithIdentifier:@"HelpViewController" sender:nil];
}

// //////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - iCloud updates
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)reloadDataWithNotification:(NSNotification *)notification {
	dispatch_async(dispatch_get_main_queue(), ^{
	    [self reloadData];
	    [self displayNotification:notification];
	});
}

- (void)displayNotification:(NSNotification *)notification {
	NSDictionary *update = [[notification object] userInfo];
	NSMutableArray *array = [NSMutableArray new];

	NSArray *actions = @[@"deleted", @"inserted", @"updated"];

	[actions enumerateObjectsUsingBlock: ^(NSString *action, NSUInteger idx, BOOL *stop) {
	    if (update[action] && [update[action] count]) {
	        NSString *requestString = NSLocalizedString(@"request", nil);
	        if ([update[action] count] == 1) {
	            requestString = NSLocalizedString(@"request", nil);
			}
	        [array addObject:[NSString stringWithFormat:NSLocalizedString(@"%@ %@ %@", nil), @([update[action] count]), requestString, action]];
		}
	}];

	NSString *string = [array componentsJoinedByString:@", "];

	if (string.length) {
		NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:string attributes:@{ NSForegroundColorAttributeName:[UIColor whiteColor], NSFontAttributeName:[UIFont boldSystemFontOfSize:14.0] }];

		CGFloat height = 128;
		CGRect frame = CGRectMake(0, CGRectGetHeight(self.view.frame) - height, CGRectGetWidth(self.view.frame), height);
		frame = CGRectInset(frame, 20, 20);
		UITextView *updateInfoView = [[UITextView alloc] initWithFrame:frame];
		[updateInfoView setTextAlignment:NSTextAlignmentCenter];
		[updateInfoView setBackgroundColor:[kColorDefault colorWithAlphaComponent:0.75]];
		[updateInfoView setAttributedText:attributedString];
		[updateInfoView setUserInteractionEnabled:NO];
		[self.view addSubview:updateInfoView];

		[updateInfoView setClipsToBounds:YES];
		[updateInfoView.layer setCornerRadius:5.0];
		[updateInfoView.layer setShadowColor:[UIColor blackColor].CGColor];
		[updateInfoView.layer setShadowOffset:CGSizeMake(0, 2)];
		[updateInfoView.layer setShadowOpacity:0.3];
		[updateInfoView.layer setShadowRadius:1.0];

		[updateInfoView setAlpha:0.0];
		[UIView animateWithDuration:0.3 animations: ^{
		    [updateInfoView setAlpha:1.0];
		} completion: ^(BOOL finished) {
		    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapView:)];
		    [updateInfoView addGestureRecognizer:tap];
		    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
		        [self removeView:updateInfoView];
			});
		}];
	}
}

- (void)tapView:(UITapGestureRecognizer *)tap {
	if (tap.state == UIGestureRecognizerStateEnded) {
		[self removeView:tap.view];
	}
}

- (void)removeView:(UIView *)view {
	[UIView animateWithDuration:0.3 animations: ^{
	    [view setAlpha:0.0];
	} completion: ^(BOOL finished) {
	    [view removeFromSuperview];
	}];
}

// //////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UITextView delegate
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)textViewDidChange:(UITextView *)textView {
	if ([textView isEqual:self.endpointTextView]) {
		self.endpointTextView.text = [self.endpointTextView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
		[[NSUserDefaults standardUserDefaults] setValue:textView.text forKey:kEndpoint];
		[[NSUserDefaults standardUserDefaults] synchronize];
	}
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
	if ([textView isEqual:self.endpointTextView]) {
		if ([text isEqualToString:@"\n"]) {
			[textView resignFirstResponder];
			return NO;
		}
	}
	return YES;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
	[self.endpointTextView resignFirstResponder];
}

@end
