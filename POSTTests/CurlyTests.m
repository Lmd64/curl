//
// CurlyTests.m
// CurlyTests
//
//  Created by Liam Dunne on 06/04/2014.
//  Copyright (c) 2014 Lmd64. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSString+KeyValues.h"

@interface CurlyTests : XCTestCase

@end

@implementation CurlyTests

- (void)setUp {
	[super setUp];
	// Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
	// Put teardown code here. This method is called after the invocation of each test method in the class.
	[super tearDown];
}

- (void)testKeypaths {
	NSArray *response = @[@{ @"key1":@{ @"key2":@"value" },
	                         @"array":@[@"item1", @"item2", @"item3"],
	                         @"numarray":@[@1, @2, @3], }];

	NSArray *keyPaths = @[
	        @".",
	        @".@count",
	        @".@max",
	        @"@",
	        @"@avg.numarray",
	        @"@count",
	        @"@max",
	        @"@max.numarray",
	        @"array.@avg",
	        @"array.@count",
	        @"array.@max",
	        @"array@avg",
	        @"array@count",
	        @"array@max",
	        @"key",
	        @"key1.",
	        @"key1.@count",
	        @"key1.key",
	        @"key1.key2",
	        @"key1.key2.@count",
	        @"numarray.@avg",
	        @"numarray.@count",
	        @"numarray.@max",
	        @"numarray.@sum",
	        @"numarray.@sum.self",
	        @"numarray@avg",
	        @"numarray@count",
	        @"numarray@max"
	    ];

	CLog(@"");

	[response enumerateObjectsUsingBlock: ^(NSDictionary *dictionary, NSUInteger idx, BOOL *stop) {
	    [keyPaths enumerateObjectsUsingBlock: ^(NSString *keyPath, NSUInteger idx, BOOL *stop) {
	        NSString *value = nil;
	        if ([keyPath validKeyPathForDictionary:dictionary]) {
	            value = [dictionary safeValueForKeyPath:keyPath];
			}
	        if ([value isKindOfClass:[NSNumber class]]) {
	            value = [NSString stringWithFormat:@"%@[%@]", [keyPath keyPath], value];
			}
	        CLog(@"%@:%@", keyPath, value);
		}];
	}];

	CLog(@"");
}

- (void)testGet{
    
}

@end
